from pathlib import Path
import torch
import numpy as np
from torchvision import transforms

PROJ_DIR = Path(__file__).parent
TARGET = torch.tensor(np.array([0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]))

normalize_img_net = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])

normalize_celeba = transforms.Normalize(mean=[0.5061, 0.4254, 0.3838],
                                 std=[0.309, 0.2886, 0.2879])

normalize = normalize_img_net
resize=224    

transform_aug = transforms.Compose([
        transforms.RandomHorizontalFlip(),
        transforms.RandomResizedCrop((resize,resize)),
        transforms.RandomRotation(degrees=25),
        transforms.ToTensor(),
        normalize
        
])
transform = transforms.Compose([
        transforms.Resize((resize,resize)),
        transforms.ToTensor(),
        normalize
        
])


def fine_tune_schedule(epoch):
    init = 1e-4
    if epoch in range(5):
        return init * (epoch + 1)
    highest = 5e-4
    return  highest / (2**(epoch-4))