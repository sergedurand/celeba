WORK IN PROGRESS, there is no guarantee whatsoever that things runs as they're supposed to be or run at all.

## Install

Install the requirements using your favourite package manager / environment set-up.
Requirement.txt should have most requirements, but I give no guarantees.
I use pytorch-lightning for training, with test-tube for logging. 

The conda env config celebA.yml can also be used to create an environment:

```
conda env create -f celebA.yml
```

You might need to change the prefix to your env directory.


## Run stuff

To do adversarial training, create a model for 40 outputs then run something like:

```python
import timm
import torch

from attacks.PGD import PGD
from config import PROJ_DIR
from utils import train_adv

model = timm.create_model("resnet50", pretrained=True, num_classes=40)
train_adv(model=model,
          name="resnet50_pretrained",
          adv_folder=PROJ_DIR / "data/celeba_subset/celeba_adv_PGD_compressed",
          root=PROJ_DIR / "data/celeba_subset",
          max_epochs=15,
          monitor="val_acc",
          resize=224,
          batch_size=8,
          freeze=False,
          threshold=0.5
          )
torch.save(model.state_dict(), "resnet50_PGD_trained.pth")

```

A simple csv log will be saved in "results/name".
Test-tube logs (containing tensorboard events) will be saved in "results/tt_logs/name"
Checkpoints will be saved in "results/name"

monitor option are "val_acc", "clean_acc", "val_loss", "clean_loss", but if using loss you need to change the early stopping criteria from max to min.

Threshold of 0.5 means that 1 out of 2 step will use adversarial samples.
Threshold of 0 means that we'll only use adversarial samples
Threshold of 1 means that we'll only use clean samples

This is for training. For validating and testing we'll use only adversarial samples.

Root should point to the data organized this way: 

```
    --- reid_dataset
        --- train.csv
        --- val.csv
        --- test.csv
    --- celeba
        --- img_align_celeba
        --- identity_celebA.txt
        --- list_attr_celeba.txt
        --- list_bbox_celeba.txt
        --- list_eval_partition.txt
        --- list_landmarks_align_celeba.txt
```

adv_folder should point to a folder containing the adversarial samples in .jpg format with the same name as original images.

img_align_celeba can contain a subset of images, the dataloader should adapt. In this case the subset should be also contained in the adversarial folder.

If freeze is True the model weights will be freezed and only the classifier will be trained (could fail for weird models)

To do adversarial training using an attack that will compute adversarial examples at every step you can supply the attack (see attacks dir for current options, FGSM, BIM, PGD, SMIA)

```python
model = timm.create_model("resnet50", pretrained=True, num_classes=40)
attack = PGD(model=model, epsilon=8. / 255, num_iterations=20, loss_fn=torch.nn.BCEWithLogitsLoss())
train_adv(model=model,
          name="resnet50_pretrained",
          attack=attack,
          root=PROJ_DIR / "data/celeba_subset",
          max_epochs=15,
          monitor="val_acc",
          resize=224,
          batch_size=8,
          freeze=False,
          threshold=0.5
          )
```

If both an adv_folder and an attack are provided, the folder prevails and adversarial samples will be loaded from the folder, not computed.

To do normal training you can use the train function with the same keywords except those related to the adversarial aspect (attack, adv_folder, monitor, threshold)


To evaluate on FGSM / BIM / PGD using the scale from the contest can run the util function:

```python
import torch
from utils import evaluate_vanilla
model = torch.load("mymodel.pth")

evaluate_vanilla(model=model,
                 step=20,
                 batch_size=32,
                 resize=224,
                 root=PROJ_DIR / "data/celeba_subset")

```

See also evaluate_attack to evaluate an attack and evaluate_def to evaluate a set of attacks (you'll need to create attacks and dataloader). Similarly as adversarial training, you can create a loader with an adv_folder argument if you want to use precomputed attacks instead of computing them for the evaluated model.

utils.py contains helper functions to create models, train and evaluate them.
train_adv contains the pytorch_lightning module for adversarial training and trainer.py the one for vanilla training.

see also save_attack.py to compute and save the jpg of adversarial samples.