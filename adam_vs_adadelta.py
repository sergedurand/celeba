import pathlib
from utils import train
from dataset import get_data_loader
import torch
import timm
from torchvision import models
from config import PROJ_DIR, transform
from models.simple_net import Net, NetFeat
from attacks.FGSM import FGSM
from attacks.PGD import PGD
from pathlib import Path
import random
from models.ensemble import Ensemble

model = Net()

train(
    model=model,
    root = PROJ_DIR / "data/celeba_subset",
    name="baseline_subset_adamax",
    max_test=1500,
    max_epochs=30,

)