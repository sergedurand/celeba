import pathlib
from dataset import get_data_loader
import torch
import timm
from torchvision import models

from config import PROJ_DIR, transform
from models.simple_net import Net, NetFeat
from attacks.FGSM import FGSM
from utils import evaluate_vanilla, train, \
    changeNumClass, train_adv, evaluate, evaluate_attack
from attacks.PGD import PGD
from pathlib import Path
import random
from models.ensemble import Ensemble
from tqdm import tqdm

# model = timm.create_model("ecaresnet101d_pruned",pretrained=True,num_classes=40)
# train(
#     model=model,
#     root= PROJ_DIR / "data/celeba_subset",
#     name="ecaresnet101d_pruned",
# )

# torch.save(model.cpu(),"ecaresnet101d_pruned.pth")

bootstrap = 50000
nb_model = 7
for i in range(nb_model):
    # model = timm.create_model("ecaresnet101d_pruned",pretrained=False,num_classes=40)
    # aux_model = torch.load("/tempory/celeba/adv_trained_models/baseline_subset_dynamic_PGD_20.pth").cuda()
    model = timm.create_model("ecaresnet101d_pruned",pretrained=True,num_classes=40)
    # attack_model = torch.load("/tempory/celeba/subset_models/baseline_subset.pth")
    # attack2 = PGD(attack_model.cuda(),torch.nn.BCEWithLogitsLoss(), num_iterations=10)
    # attack_model = torch.load("/tempory/celeba/adv_trained_models/baseline_subset_dynamic_PGD_20.pth")
    # attack3 = PGD(attack_model.cuda(),torch.nn.BCEWithLogitsLoss(), num_iterations=10)
    val_atk = PGD(model, torch.nn.BCEWithLogitsLoss(), num_iterations=20)
    atk = PGD(model, torch.nn.BCEWithLogitsLoss(), num_iterations=2)

    train_adv(
        model=model,
        root = PROJ_DIR / "data/celeba_subset",
        bootstrap = None,
        name="ecaresnet101d_pruned_ATTA_PGD_2_reset_5_t0_no_sum{}".format(i),
        max_test=1000,
        val_atk=atk,
        attack=atk,
        sum_loss=False,
        threshold=0,
        save_attack=True,
        monitor="adv_val_loss",
        max_epochs=100,
        batch_size=16,
        weights=(1,1),
        accumulate_grad_batches=1,
        reset=5,
        saved_pert_folder=PROJ_DIR / "data/ATTA",
    )

    torch.save(model.cpu(),"ecaresnet101d_pruned_ATTA_PGD_2_reset_5_t0_no_sum{}.pth".format(i))
    break

# model = torch.load("/tempory/celeba/baseline_PGD_20_t0_no_sum0.pth").cuda()
model = torch.load("ecaresnet101d_pruned_ATTA_PGD_2_reset_5_t0_no_sum0.pth")
evaluate_vanilla(model.cuda(), step=20,limit=1000)
# model = torch.load("/tempory/celeba/baseline_subset_adv_PGD_20_transferred_all_data_resnext101.pth")
# evaluate_vanilla(model.cuda(), step=20,limit=1000)



# models = Path("baseline_ATTA_bootstrap").glob("*.pth")
# models = [torch.load(m) for m in models]
# attack = PGD(models[2], torch.nn.BCEWithLogitsLoss(),num_iterations=50,to_copy=True)
# total = 0
# for m in models:
#     total += evaluate(m, root= PROJ_DIR / "data/celeba_subset")
# print(total / len(models))
# folders = list()
# dirs = Path("data").glob("*")
# for d in dirs:
#     if d.is_dir() and len(list(d.glob("*.jpg"))) > 2000 and "ATTA" not in str(d):
#         folders.append(str(d))
# loader = get_data_loader(32,"test",num_workers=3, root=PROJ_DIR / "data/celeba_subset", resize=224, adv_folder=folders)
# # evaluate_attack(models[0],loader,attack,limit=2000, verbose=True)
# # evaluate_attack(models[2],loader,attack,limit=2000, verbose=True)

# ensemble = Ensemble(models, mode="vote")
# evaluate(ensemble, root = PROJ_DIR / "data/celeba_subset")
# attack_model = torch.load("/tempory/celeba/adv_trained_models/baseline_subset_dynamic_PGD_20.pth")
# evaluate(attack_model, root= PROJ_DIR / "data/celeba_subset")
# attack = PGD(attack_model, torch.nn.BCEWithLogitsLoss())
# evaluate_attack(ensemble, loader, attack=attack, verbose=True, limit=1000)

# attack_model = torch.load("/tempory/celeba/subset_models/baseline_subset.pth")
# evaluate(attack_model, root= PROJ_DIR / "data/celeba_subset")
# attack = PGD(attack_model, torch.nn.BCEWithLogitsLoss())
# evaluate_attack(ensemble, loader, attack=attack, verbose=True, limit=1000)
