## Rules:

Regarding rule 9 : 
> The defence models should not produce random outputs for the same input at different points of time. Participants should make sure that their models are deterministic.

- Does this apply to input preprocessing? E.g defenses based on adding random noise/rotation/cropping of the input are excluded?
- Does this apply only for the inference part? I.e. can we use random data augmentation during training

Regarding evaluation:
- Is the clean accuracy or adversarial accuracy taken into account to ranked defenses appart from the computation of the delta score?
For example will a model with 81 % clean, 80 % adv be ranked higher than a model with 90 % clean / 85 % adv ?