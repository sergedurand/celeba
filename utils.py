from attacks.SMIA import SMIA
import copy
import pytorch_lightning as pl
from pytorch_lightning.loggers.test_tube import TestTubeLogger
from pytorch_lightning.loggers import CSVLogger, \
    TensorBoardLogger, \
    TestTubeLogger
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks import Callback
from pytorch_lightning.loggers.csv_logs import CSVLogger
from timm.models.helpers import resume_checkpoint
import torch
from torch.nn.modules import loss
from tqdm import tqdm

from trainer import CelebAModel
from dataset import create_attribute_dataloader, \
    get_data_loader
from attacks.PGD import PGD
from attacks.FGSM import FGSM
from attacks.BIM import BIM
from trainer_adv import CelebA_adv
from config import PROJ_DIR
from torchvision import transforms

def _changeNumClass(classifier, num_class, cpt=0):
    if isinstance(classifier, torch.nn.Linear):
        classifier = torch.nn.Linear(classifier.in_features, num_class)
        return classifier
    elif isinstance(classifier, torch.nn.Sequential):
        if isinstance(classifier[-1], torch.nn.Linear):
            classifier[-1] = torch.nn.Linear(classifier[-1].in_features, num_class)
            return classifier
    if cpt > 1:
        return classifier
    print("The classifier is not a linear module, trying to go further")
    class_name = [n for n, _ in classifier.named_children()][-1]
    if class_name == "fc":
        classifier.fc = _changeNumClass(classifier.fc, num_class, cpt + 1)
    elif class_name == "classifier":
        classifier.classifier = _changeNumClass(classifier.classifier, num_class, cpt + 1)
    elif class_name == "head":
        classifier.head = _changeNumClass(classifier.head, num_class, cpt + 1)
    elif class_name == "classif":
        classifier.classif = _changeNumClass(classifier.classif, num_class, cpt + 1)
    else:
        classifier.classif = torch.nn.Sequential(classifier.classif, torch.nn.Linear(1000, num_class))
        print("Couldn't find the classifier")
    return classifier


def changeNumClass(model, num_class, cpt=0):
    class_name = [n for n, _ in model.named_children()][-1]
    if class_name == "fc":
        model.fc = _changeNumClass(model.fc, num_class, cpt)
    elif class_name == "classifier":
        model.classifier = _changeNumClass(model.classifier, num_class, cpt)
    elif class_name == "head":
        model.head = _changeNumClass(model.head, num_class, cpt)
    elif class_name == "classif":
        model.classif = _changeNumClass(model.classif, num_class, cpt)
    else:
        print("Couldn't find the classifier")
    return model


def check_model_size(model):
    """
    Utility function to check the number of parameters in the model. Prints the value in Millions
    :param model: Input model to be checked for size
    :return: None
    """
    num_params = 0
    traininable_param = 0
    for param in model.parameters():
        num_params += param.numel()
        if param.requires_grad:
            traininable_param += param.numel()
    print("[Network  Total number of parameters : %.3f M" % (num_params / 1e6))
    print(
            "[Network  Total number of trainable parameters : %.3f M"
            % (traininable_param / 1e6)
    )


def train(model,
          name,
          max_epochs=15,
          monitor="val_loss",
          resize=224,
          batch_size=32,
          root=PROJ_DIR / "data",
          freeze=False,
          resume_checkpoint=None,
          sgd=False,
          lr=1e-3,
          loss_fn=torch.nn.BCEWithLogitsLoss(),
          train_transform=None,
          test_transform=None,
          save_intermediate=False,
          accumulate_grad_batch=4,
          tpu_cores=None,
          bootstrap=None,
          max_test=None):
    logger1 = CSVLogger(save_dir=str(PROJ_DIR / "results"), name=name)
    logger2 = TestTubeLogger(save_dir=str(PROJ_DIR / "results/tt_logs"), name=name)
    callbacks = [EarlyStopping(monitor=monitor, patience=6, min_delta=1e-4, mode="min")]
    if save_intermediate:
        callbacks.append(SaveModel(name))
    if tpu_cores is None:
        trainer = pl.Trainer(gpus=1, max_epochs=max_epochs, progress_bar_refresh_rate=1, val_check_interval=0.5,
                             default_root_dir="results/" + name, logger=[logger1, logger2],
                             callbacks=callbacks,
                             accumulate_grad_batches=accumulate_grad_batch,
                             resume_from_checkpoint=resume_checkpoint)
    else:
        trainer = pl.Trainer(tpu_cores=tpu_cores, max_epochs=max_epochs, progress_bar_refresh_rate=1, val_check_interval=0.5,
                             default_root_dir="results/" + name, logger=[logger1, logger2],
                             callbacks=callbacks,
                             accumulate_grad_batches=accumulate_grad_batch,
                             resume_from_checkpoint=resume_checkpoint)
    celebModel = CelebAModel(model=model,
                             resize=resize,
                             batch_size=batch_size,
                             freeze=freeze,
                             root=root,
                             lr=lr,
                             sgd=sgd,
                             loss_fn=loss_fn,
                             train_transform=train_transform,
                             test_transform=test_transform,
                             bootstrap=bootstrap,
                             max_test=max_test)
    print(celebModel.hparams)
    print("Size train = {}".format(len(celebModel.train_dataloader().dataset)))
    print("Size val = {}".format(len(celebModel.val_dataloader().dataset)))
    print("Size test = {}".format(len(celebModel.test_dataloader().dataset)))
    trainer.fit(celebModel)
    res = trainer.test(celebModel)
    print(res)


def train_adv(model, name, attack=None, max_epochs=15, monitor="adv_val_loss", resize=224, batch_size=32, root="data",
              freeze=False,
              adv_folder=None,
              threshold=0.5,
              resume_checkpoint=None,
              train_transform=None,
              test_transform=None,
              accumulate_grad_batches=4,
              save_intermediate=False,
              val_atk=None,
              sum_loss=False,
              optimize_feat=False,
              weights=(1,1),
              max_test=None,
              bootstrap=None,
              save_attack=False,
              saved_pert_folder=None,
              reset=5,
              lr=None,
              early_stopping=True):
    logger1 = CSVLogger(save_dir=str(PROJ_DIR / "results"), name=name)
    logger2 = TestTubeLogger(save_dir=str(PROJ_DIR / "results/tt_logs"), name=name)
    if early_stopping:
        callbacks = [EarlyStopping(monitor=monitor, patience=5, min_delta=1e-5, mode="min")]
    else:
        callbacks = None
    if save_intermediate:
        callbacks.append(SaveModel(name))
    trainer = pl.Trainer(gpus=1, max_epochs=max_epochs,
                         progress_bar_refresh_rate=1, val_check_interval=0.5,
                         default_root_dir="results/" + name, logger=[logger1, logger2],
                         callbacks=callbacks,
                         accumulate_grad_batches=accumulate_grad_batches,
                         resume_from_checkpoint=resume_checkpoint,
                         )
    celebModel = CelebA_adv(model, adv_folder=adv_folder, attack=attack, resize=resize, batch_size=batch_size,
                            freeze=freeze, root=root, threshold=threshold,
                            train_transform=train_transform,
                            test_transform=test_transform,
                            val_atk=val_atk,
                            sum_loss=sum_loss,
                            optimize_feat=optimize_feat,
                            weights=weights,
                            max_test=max_test,
                            bootstrap=bootstrap,
                            save_attack=save_attack,
                            saved_pert_folder=saved_pert_folder,
                            reset=reset,
                            lr=lr)
    print(celebModel.hparams)
    print("Size train = {}".format(len(celebModel.train_dataloader().dataset)))
    print("Size val = {}".format(len(celebModel.val_dataloader().dataset)))
    print("Size test = {}".format(len(celebModel.test_dataloader().dataset)))

    trainer.fit(celebModel)
    res = trainer.test(celebModel)
    print(res)

def test_PGD(model, batch_size=16, resize=224, root="data/celeba_subset", step=10, only_test=True):
    train_loader, val_loader, test_loader = create_attribute_dataloader(batch_size, resize=resize, root=root, adv=None)
    print("Size train = {}".format(len(train_loader.dataset)))
    print("Size val = {}".format(len(val_loader.dataset)))
    print("Size test = {}".format(len(test_loader.dataset)))
    attack = PGD(model=model, loss_fn=torch.nn.BCEWithLogitsLoss(), epsilon=8 / 255, num_iterations=step)
    test_adv_acc = 0
    test_clean_acc = 0
    cpt = 0
    model = model.cuda()
    for batch in tqdm(test_loader):
        indices, imgs, labels = batch
        labels = labels.to(torch.float)
        imgs = imgs.cuda()
        labels = labels.cuda()
        model.train()
        adv_imgs = attack.attack(imgs, labels)
        model.eval()
        # adv metrics
        logits = model(adv_imgs)
        adv_acc = (logits > 0).eq(labels).sum().item() / 40
        adv_acc = adv_acc / adv_imgs.shape[0]
        test_adv_acc += adv_acc
        logits = model(imgs)
        clean_acc = (logits > 0).eq(labels).sum().item() / 40
        clean_acc = clean_acc / imgs.shape[0]
        test_clean_acc += clean_acc
        cpt += 1

    print("Test clean acc = {:.4f} %".format(100 * test_clean_acc / cpt))
    print("Test adv acc = {:.4f} %".format(100 * test_adv_acc / cpt))

    if not only_test:
        train_adv_acc = 0
        train_clean_acc = 0
        cpt = 0
        model = model.cuda()
        for batch in tqdm(train_loader):
            indices, imgs, labels = batch
            labels = labels.to(torch.float)
            imgs = imgs.cuda()
            labels = labels.cuda()
            model.train()
            adv_imgs = attack.attack(imgs, labels)
            model.eval()
            # adv metrics
            logits = model(adv_imgs)
            adv_acc = (logits > 0).eq(labels).sum().item() / 40
            adv_acc = adv_acc / adv_imgs.shape[0]
            train_adv_acc += adv_acc
            logits = model(imgs)
            clean_acc = (logits > 0).eq(labels).sum().item() / 40
            clean_acc = clean_acc / imgs.shape[0]
            train_clean_acc += clean_acc
            cpt += 1

        print("Train clean acc = {:.4f} %".format(100 * train_clean_acc / cpt))
        print("Tain adv acc = {:.4f} %".format(100 * train_adv_acc / cpt))
    return 100 * test_clean_acc / cpt, 100 * test_adv_acc / cpt


def evaluate_attack(model, loader, attack=None, limit=None, verbose=False):
    test_adv_acc = 0
    test_clean_acc = 0
    nb_batch = 0
    nb_samples = 0
    model = model.cuda()
    load_attack = False
    separate_adv = None
    model.eval()
    with torch.no_grad():
        for batch in tqdm(loader):
            if len(batch) == 3:
                indices, imgs, labels = batch
            elif len(batch) == 4:
                indices, imgs, adv_imgs, labels = batch
                load_attack = attack is None
            labels = labels.to(torch.float)
            imgs = imgs.cuda()
            labels = labels.cuda()
            logits = model(imgs)
            if isinstance(logits, tuple) or isinstance(logits, list):
                logits = logits[0]
            clean_acc = (logits > 0).eq(labels).sum().item() / 40
            clean_acc = clean_acc / imgs.shape[0]
            test_clean_acc += clean_acc
            if not load_attack:
                with torch.enable_grad():
                    adv_imgs = attack.attack(imgs, labels)

            if isinstance(adv_imgs, list):  # several attacks loaded
                if separate_adv is None:
                    separate_adv = [0] * len(adv_imgs)
                adv_acc = 0
                for i, loaded_imgs in enumerate(adv_imgs):
                    logits = model(loaded_imgs.cuda())
                    if isinstance(logits, tuple) or isinstance(logits, list):
                        logits = logits[0]
                    _adv_acc = (logits > 0).eq(labels).sum().item() / 40
                    _adv_acc = _adv_acc / loaded_imgs.shape[0]
                    separate_adv[i] += _adv_acc
                    adv_acc += _adv_acc
                adv_acc /= len(adv_imgs)

            else:
                # one attack only
                adv_imgs = adv_imgs.cuda()
                logits = model(adv_imgs)
                if isinstance(logits, list) or isinstance(logits, tuple):
                    logits = logits[0]
                adv_acc = (logits > 0).eq(labels).sum().item() / 40
                adv_acc = adv_acc / adv_imgs.shape[0]
            test_adv_acc += adv_acc
            nb_batch += 1
            nb_samples += imgs.shape[0]
            if limit is not None and nb_samples > limit:
                break
    if verbose:
        print("Clean = {:.2f} %".format(100 * test_clean_acc / nb_batch))
        print("Adv = {:.2f} %".format(100 * test_adv_acc / nb_batch))
        if separate_adv is not None:
            folder_names = [folder.split("/")[-1] for folder in loader.dataset.adv_folder]
            separate_adv = [100 * total / nb_batch for total in separate_adv]
            for i in range(len(separate_adv)):
                print("Adv for attack {} : {:.2f} %".format(folder_names[i],separate_adv[i]))
            return 100 * test_clean_acc / nb_batch, 100 * test_adv_acc / nb_batch, separate_adv

    return 100 * test_clean_acc / nb_batch, 100 * test_adv_acc / nb_batch


def evaluate_def(model, attacks, loader, coefs=None, limit=None):
    if coefs is not None:
        assert len(coefs) == len(attacks)
        assert sum(coefs) == 1
    score = 0
    scores = list()
    for i, attack in enumerate(attacks):
        clean, adv = evaluate_attack(model, attack=attack, loader=loader, limit=limit)
        print("clean acc = {:.4f} %".format(clean))
        delta = clean - adv
        scores.append(adv)
        print("Acc for {} = {:.4f}".format(type(attack).__name__, adv))
        print("Delta for {} = {:.4f}".format(type(attack).__name__, delta))
        if coefs is not None:
            delta = delta * coefs[i]
        score += delta
    if coefs == None:
        score /= len(attacks)

    return score, scores


def evaluate_vanilla(model, step=10, batch_size=16, resize=224, root=PROJ_DIR / "data/celeba_subset", limit=None):
    coefs = [0.2, 0.4, 0.4]
    loss_fn = torch.nn.BCEWithLogitsLoss()
    attacks = [FGSM(copy.deepcopy(model), save_folder=None, loss_fn=loss_fn),
               BIM(copy.deepcopy(model), save_folder=None, loss_fn=loss_fn, num_iterations=step),
               PGD(copy.deepcopy(model), loss_fn=loss_fn, num_iterations=step)]
    train_loader, val_loader, test_loader = create_attribute_dataloader(batch_size, resize=resize, root=root)
    final_score, scores = evaluate_def(model, attacks, test_loader, coefs, limit=limit)
    print("Delta final = {:.4f}".format(final_score))
    return final_score, scores


def evaluate_vanilla_SMIA(model, step=10, batch_size=16, resize=224, root=PROJ_DIR / "data/celeba_subset"):
    coefs = [0.2, 0.4, 0.4]
    loss_fn = torch.nn.BCEWithLogitsLoss()
    attacks = [FGSM(model, save_folder=None, loss_fn=loss_fn),
               BIM(model, save_folder=None, loss_fn=loss_fn, num_iterations=step),
               PGD(model, loss_fn=loss_fn, num_iterations=step),
               SMIA(model, loss_fn=loss_fn, num_iterations=step, a2=0),
               SMIA(model, loss_fn=loss_fn, num_iterations=step, a2=0)]
    train_loader, val_loader, test_loader = create_attribute_dataloader(batch_size, resize=resize, root=root)
    final_score = evaluate_def(model, attacks, test_loader, coefs)
    print("Delta final = {:.4f}".format(final_score))
    return final_score


def test_checkpoint(model, checkpoint_path):
    celebModel = CelebA_adv.load_from_checkpoint(model=model, attack=None, checkpoint_path=checkpoint_path)
    model = celebModel.model
    test_PGD(model)
    test_PGD(model, step=40)


def evaluate(model, loader=None, root=None, limit=None, resize=224, verbose=True):
    if loader is None and root is not None:
        loader = get_data_loader(4, "test", shuffle=False, num_workers=4, root=root, resize=resize)
    test_clean_acc = 0
    nb_batch = 0
    nb_samples = 0
    model = model.cuda()
    model.eval()
    with torch.no_grad():
        for batch in tqdm(loader):
            indices, imgs, labels = batch
            labels = labels.to(torch.float)
            imgs = imgs.cuda()
            labels = labels.cuda()
            logits = model(imgs)
            clean_acc = (logits > 0).eq(labels).sum().item() / 40
            clean_acc = clean_acc / imgs.shape[0]
            test_clean_acc += clean_acc
            nb_batch += 1
            nb_samples += imgs.shape[0]
            if limit is not None and nb_samples > limit:
                break
    if verbose:
        print("Clean = {:.2f} %".format(100 * test_clean_acc / nb_batch))
    return 100 * test_clean_acc / nb_batch


normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])


def simple_transform(resize=224):
    return transforms.Compose([
        transforms.Resize((resize, resize)),
        transforms.ToTensor()
    ])


def augmented_transform(resize=224):
    return transforms.Compose([
        transforms.RandomHorizontalFlip(),
        transforms.RandomResizedCrop((resize, resize)),
        transforms.ToTensor(),
        normalize

    ])

class SaveModel(Callback):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def on_epoch_end(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        torch.save(copy.deepcopy(pl_module.model).cpu(),
                   PROJ_DIR / "{}_{}.pth".format(self.name, pl_module.current_epoch))



