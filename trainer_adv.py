"""
inspired by https://colab.research.google.com/github/PytorchLightning/pytorch-lightning/blob/master/notebooks/01-mnist-hello-world.ipynb#scrollTo=4DNItffri95Q
"""
from models.autoencoder import AutoEncoderDefenceNetwork
from attacks.PGD import PGD
from attacks.abstract_attacks import AbstractAttack
from torch.utils.data import Subset

from config import PROJ_DIR, \
    fine_tune_schedule
import pytorch_lightning as pl
from pytorch_lightning.metrics.functional import accuracy
from torch import nn
import torch
from attacks.PGD import PGD
from dataset import create_attribute_dataloader, PertFolder
import numpy as np
import random
from PIL import Image


class CelebA_adv(pl.LightningModule):
    def __init__(self, model, attack: AbstractAttack = None, loss_fn=nn.BCEWithLogitsLoss(), freeze=False,
                 batch_size=32, resize=None, root=PROJ_DIR / "data", threshold=0.5, adv_folder=None, val_atk=None,
                 train_transform=None,
                 test_transform=None,
                 sum_loss=False,
                 random_adv=True,
                 optimize_feat=False,
                 weights=(1, 1),
                 max_test=None,
                 bootstrap=None,
                 save_attack=False,
                 reset=5,
                 saved_pert_folder=None,
                 fast_schedule=None,
                 lr=None):
        super().__init__()
        self.model = model
        self.classifier = [n for n, _ in self.model.named_children()][-1]
        # print("Classifier is {}, type : {}".format(self.classifier,type(list(self.model.named_children())[-1][-1]).__name__))
        self.loss_fn = loss_fn
        self.freeze = freeze
        self.batch_size = batch_size
        self.resize = resize
        self.adv_folder = adv_folder
        self.loaders = create_attribute_dataloader(self.batch_size, resize=resize, root=root,
                                                   adv_folder=self.adv_folder, 
                                                   train_transform=train_transform,
                                                   test_transform=test_transform,
                                                   bootstrap=bootstrap)
        if max_test is not None:
            n = len(self.loaders[1].dataset)
            new_idx = random.sample(range(n),k=max_test)
            self.loaders[1].dataset.data = Subset(self.loaders[1].dataset.data,new_idx)
            n = len(self.loaders[2].dataset)
            new_idx = random.sample(range(n), k=max_test)
            self.loaders[2].dataset.data = Subset(self.loaders[2].dataset.data, new_idx)

        self.filenames = self.loaders[0].dataset.data.filename
        self.threshold = threshold
        self.size = len(self.train_dataloader().dataset)
        self.atk = attack
        self.val_atk = val_atk
        self.sum_loss = sum_loss
        self.feat_loss = None if not optimize_feat else torch.nn.MSELoss()
        self.weights = weights
        self.save_hyperparameters("batch_size", "loss_fn", "freeze", "resize", "root", "threshold",
                                  "sum_loss", "weights", "optimize_feat")
        self.saved_attack = None
        self.saved_attack_folder = saved_pert_folder
        if saved_pert_folder is not None:
            self.saved_attack = PertFolder(dir=saved_pert_folder,filename=self.filenames)
        elif save_attack:
            self.saved_attack = torch.zeros(self.size,3,resize,resize)
        self.reset = reset
        self.fast_schedule=fast_schedule
        self.lr = lr

    def forward(self, x, clean=None):
        if isinstance(self.model, AutoEncoderDefenceNetwork):
            return self.model(x, clean)
        return self.model(x)

    def training_step(self, batch, batch_nb):
        if self.adv_folder is None:
            idx, imgs, labels = batch
            adv_imgs = None
        else:
            idx, imgs, adv_imgs, labels = batch
        if isinstance(self.loss_fn, nn.BCEWithLogitsLoss):
            labels = labels.to(torch.float)
        t = np.random.rand()
        if isinstance(self.model, AutoEncoderDefenceNetwork):
            if self.adv_folder is None:
                if isinstance(self.atk, list):
                    atk = random.choice(self.atk)
                    adv_imgs = atk.attack(imgs, labels)
                else:
                    adv_imgs = self.atk.attack(imgs, labels)
            loss = self.loss_fn(self(adv_imgs, imgs), labels)
            return loss
        if t > self.threshold:
            if len(batch) > 3:
                if not isinstance(adv_imgs, list):
                    adv_imgs = [adv_imgs]
                n = len(adv_imgs) if self.atk is None else len(adv_imgs) + 1
                idx = np.random.randint(n)
                if idx < len(adv_imgs):
                    adv_imgs = adv_imgs[idx]
                else:
                    self.model.eval()
                    if isinstance(self.atk, list):
                        atk = random.choice(self.atk)
                        adv_imgs = atk.attack(imgs, labels)
                    else:
                        adv_imgs = self.atk.attack(imgs, labels)
                    self.model.train()
            else:
                self.model.eval()
                _imgs = imgs
                if self.current_epoch > 1 and self.saved_attack is not None and (self.current_epoch % self.reset != 0):
                    if isinstance(self.saved_attack, torch.Tensor):
                        _imgs = self.saved_attack[idx].cuda()
                    else:
                        _imgs = [self.saved_attack[i].cuda() for i in idx]
                        _imgs = torch.stack(_imgs)
                if isinstance(self.atk, list):
                    atk = random.choice(self.atk)
                    adv_imgs = atk.attack(_imgs, labels)
                else:
                    adv_imgs = self.atk.attack(_imgs, labels)
                    self.model.train()
                if self.current_epoch >= 1 and self.saved_attack is not None:
                    if self.saved_attack_folder is not None:
                        for i, img in enumerate(adv_imgs):
                            img = img.clone().detach().cpu().numpy()
                            array = img.transpose(1,2,0) * 255
                            array = array.astype(np.uint8)
                            save_img = Image.fromarray(array)
                            save_name = self.saved_attack_folder / self.filenames[idx[i]]
                            save_img.save(save_name)
                    else:
                        self.saved_attack[idx] = adv_imgs.clone().detach().cpu()


        if self.sum_loss and adv_imgs is not None:
            logits = self(imgs)
            if isinstance(adv_imgs,list):
                adv_imgs = random.choice(adv_imgs)
            logits2 = self(adv_imgs)
            if isinstance(logits, tuple):
                out1, feat1 = logits
                out2, feat2 = logits2
                loss = self.loss_fn(out1, labels) + self.weights[0] * self.loss_fn(out2, labels)
            else:
                loss = self.loss_fn(logits, labels) + self.weights[0] * self.loss_fn(logits2, labels)
            if self.feat_loss is not None:
                loss += self.weights[1] * self.feat_loss(feat1, feat2)
        else:
            if t > self.threshold:
                loss = self.loss_fn(self(adv_imgs), labels)
            else:
                loss = self.loss_fn(self(imgs), labels)
        self.log("train_loss",loss,prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        total_loss = None
        with torch.no_grad():
            if self.adv_folder is None:
                _, imgs, labels = batch
            else:
                _, imgs, adv_imgs, labels = batch
            if isinstance(self.loss_fn, nn.BCEWithLogitsLoss):
                labels = labels.to(torch.float)
            # adv metrics
            with torch.enable_grad():
                if self.val_atk is not None:
                    adv_imgs = self.val_atk.attack(imgs, labels)
                elif self.atk is not None:
                    if isinstance(self.atk, list):
                        atk = random.choice(self.atk)
                        adv_imgs = atk.attack(imgs, labels)
                    else:
                        adv_imgs = self.atk.attack(imgs, labels)
                elif isinstance(adv_imgs, list):
                    adv_imgs = adv_imgs[0]
            logits = self(adv_imgs)
            if isinstance(logits, tuple):
                logits, features1 = logits
            adv_loss = self.loss_fn(logits, labels)
            adv_acc = (logits >= 0).eq(labels).sum().item() / 40
            adv_acc = adv_acc / adv_imgs.shape[0]
            # clean metrics
            logits = self(imgs)
            if isinstance(logits, tuple):
                logits, features2 = logits
            clean_loss = self.loss_fn(logits, labels)
            clean_acc = (logits >= 0).eq(labels).sum().item() / 40
            clean_acc = clean_acc / imgs.shape[0]
            self.log('adv_val_loss', adv_loss, prog_bar=True)
            self.log('adv_val_acc', adv_acc, prog_bar=True)
            self.log('val_loss', adv_loss, prog_bar=True)
            self.log('val_acc', adv_acc, prog_bar=True)
            self.log('clean_val_loss', clean_loss, prog_bar=True)
            self.log('clean_val_acc', clean_acc, prog_bar=True)
            if self.sum_loss:
                total_loss = clean_loss + self.weights[0] * adv_loss
            if self.feat_loss is not None:
                total_loss += self.weights[1] * self.feat_loss(features1, features2)
            if total_loss is not None:
                self.log("val_summed_loss", total_loss, prog_bar=True)
                return adv_loss, adv_acc, clean_loss, clean_acc, total_loss
        return adv_loss, adv_acc, clean_loss, clean_acc

    def test_step(self, batch, batch_idx):
        total_loss = None
        with torch.no_grad():
            if self.adv_folder is None:
                _, imgs, labels = batch
            else:
                _, imgs, adv_imgs, labels = batch
            if isinstance(self.loss_fn, nn.BCEWithLogitsLoss):
                labels = labels.to(torch.float)
            # adv metrics
            with torch.enable_grad():
                if self.val_atk is not None:
                    adv_imgs = self.val_atk.attack(imgs, labels)
                elif self.atk is not None:
                    adv_imgs = self.atk.attack(imgs, labels)
                elif isinstance(adv_imgs, list):
                    adv_imgs = adv_imgs[0]
            logits = self(adv_imgs)
            if isinstance(logits, tuple):
                logits, features1 = logits
            adv_loss = self.loss_fn(logits, labels)
            adv_acc = (logits >= 0).eq(labels).sum().item() / 40
            adv_acc = adv_acc / adv_imgs.shape[0]
            # clean metrics
            logits = self(imgs)
            if isinstance(logits, tuple):
                logits, features2 = logits
            clean_loss = self.loss_fn(logits, labels)
            clean_acc = (logits >= 0).eq(labels).sum().item() / 40
            clean_acc = clean_acc / imgs.shape[0]
            self.log('adv_test_loss', adv_loss, prog_bar=True)
            self.log('adv_test_acc', adv_acc, prog_bar=True)
            self.log('clean_test_loss', clean_loss, prog_bar=True)
            self.log('clean_test_acc', clean_acc, prog_bar=True)
            if self.sum_loss:
                total_loss = clean_loss + self.weights[0] * adv_loss
            if self.feat_loss is not None:
                total_loss += self.weights[1] * self.feat_loss(features1, features2)
            if total_loss is not None:
                self.log("Test summed loss", total_loss, prog_bar=True)
                return adv_loss, adv_acc, clean_loss, clean_acc, total_loss
        return adv_loss, adv_acc, clean_loss, clean_acc

    def configure_optimizers(self):
        if self.freeze:
            for name, params in self.model.named_modules():
                if name in ["fc", "classif", "head", "classifier", "last_linear", "conv_head"]:
                    for _params in params.parameters():
                        _params.requires_grad = True
                    continue
                for _name, trained_params in params.named_parameters():
                    trained_params.requires_grad = False
            print("Only optimizing classifier")
            if self.classifier == "fc":
                optim = torch.optim.AdamW(self.model.fc.parameters())
            elif self.classifier == "head":
                optim = torch.optim.AdamW(self.model.head.parameters())
            elif self.classifier == "classifier":
                optim = torch.optim.AdamW(self.model.classifier.parameters())

        else:
            print("Optimizing everything")
            if self.lr is not None:
                optim = torch.optim.AdamW(self.parameters(), lr=self.lr)
            else:
                optim = torch.optim.AdamW(self.parameters())
        if self.fast_schedule is not None:
            scheduler = torch.optim.lr_scheduler.LambdaLR(optim, fine_tune_schedule)
            return optim, scheduler
        return optim
    def train_dataloader(self):
        return self.loaders[0]

    def val_dataloader(self):

        return self.loaders[1]

    def test_dataloader(self):
        return self.loaders[2]

    def reset_loaders(self, root, batch_size=None, resize=None):
        resize = self.resize if resize is None else resize
        batch_size = self.batch_size if batch_size is None else batch_size
        self.loaders = create_attribute_dataloader(self.batch_size, resize=resize, root=root)
