from config import PROJ_DIR
from dataset import get_data_loader
from utils import changeNumClass
from torchvision import models
import torch


model = models.resnext101_32x8d(pretrained=True)
model = changeNumClass(model, 40)
model = torch.load("/tempory/celeba/all_data_resnext101.pth")
model = model.cuda()
model.eval()

import os
import argparse
import torch
import torch.nn as nn
import torchvision.datasets as datasets
import torch.utils.data as data
import torchvision.transforms as transforms



# load data
test_loader = get_data_loader(1000,"test",root=PROJ_DIR / "data/celeba_subset",resize=224)
save_dir = PROJ_DIR / "data/celeba_subset/test_autoattack"
norm = "Linf"
epsilon = 0.03
log_path = "results/autoattack"
version = "standard"
individual = True
batch_size = 1
n_ex = 1000

# create save dir
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

# load attack    
from autoattack import AutoAttack
adversary = AutoAttack(model, norm=norm, eps=epsilon, log_path=log_path,
    version=version)

l = [x for (_, x, y) in test_loader]
x_test = torch.cat(l, 0)
l = [y for (_, x, y) in test_loader]
y_test = torch.cat(l, 0)
print(len(x_test))

# example of custom version
if version == 'custom':
    adversary.attacks_to_run = ['apgd-ce', 'fab']
    adversary.apgd.n_restarts = 2
    adversary.fab.n_restarts = 2

# run attack and save images
with torch.no_grad():
    if not individual:
        adv_complete = adversary.run_standard_evaluation(x_test[:n_ex], y_test[:n_ex],
            bs=batch_size)
        
        torch.save({'adv_complete': adv_complete}, '{}/{}_{}_1_{}_eps_{:.5f}.pth'.format(
            save_dir, 'aa', version, adv_complete.shape[0], epsilon))

    else:
        # individual version, each attack is run on all test points
        adv_complete = adversary.run_standard_evaluation_individual(x_test[:n_ex],
            y_test[:n_ex], bs=batch_size)
        
        torch.save(adv_complete, '{}/{}_{}_individual_1_{}_eps_{:.5f}_plus_{}_cheap_{}.pth'.format(
            save_dir, 'aa', version, n_ex, epsilon))
                
