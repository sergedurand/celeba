from utils import changeNumClass
import torchvision
from trainer import CelebAModel

import torch 

# model = torchvision.models.resnext101_32x8d()
# model = changeNumClass(model,40)
# model = CelebAModel.load_from_checkpoint(model=model,checkpoint_path="results/all_data_resnext101/all_data_resnext101_all_data_resnext101/0_0/checkpoints/epoch=2-step=13225.ckpt")
# model = model.model.cpu()
# torch.save(model, "all_data_resnext101.pth")

model = torchvision.models.googlenet(pretrained=True)
model = changeNumClass(model,40)
model = CelebAModel.load_from_checkpoint(model=model,checkpoint_path="results/all_data_googlenet/all_data_googlenet_all_data_googlenet/0_0/checkpoints/epoch=4-step=21365.ckpt")
model = model.model.cpu()
torch.save(model, "all_data_googlenet.pth")