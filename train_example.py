import timm
import torch

from attacks.PGD import PGD
from config import PROJ_DIR
from dataset import get_data_loader
from utils import train_adv, \
    evaluate_vanilla

model = timm.create_model("resnet50", pretrained=True, num_classes=40)
train_adv(model=model,
          name="resnet50_pretrained",
          adv_folder=PROJ_DIR / "data/celeba_subset/celeba_adv_PGD_compressed",
          root=PROJ_DIR / "data/celeba_subset",
          max_epochs=15,
          monitor="val_acc",
          resize=224,
          batch_size=8,
          freeze=False,
          threshold=0.5
          )
torch.save(model.state_dict(), "resnet50_PGD_trained.pth")

model = timm.create_model("tf_efficientnetv2_s_in21k", pretrained=True, num_classes=40)
attack = PGD(model=model, epsilon=8. / 255, num_iterations=20, loss_fn=torch.nn.BCEWithLogitsLoss())
train_adv(model=model,
          name="resnet50_pretrained",
          attack=attack,
          root=PROJ_DIR / "data/celeba_subset",
          max_epochs=15,
          monitor="val_acc",
          resize=224,
          batch_size=8,
          freeze=False,
          threshold=0.5
          )

torch.save(model.state_dict(), "resnet50_PGD_trained.pth")

loader = get_data_loader(batch_size=2,
                         split="test",
                         shuffle=True,
                         num_workers=4,
                         resize=224,
                         root=PROJ_DIR / "data/celeba_subset",
                         adv_folder="/tempory/celeba/data/celeba_subset/celeba_adv_PGD_compressed")

model = torch.load("/tempory/celeba/adv_trained_models/baseline_subset_dynamic_PGD_20.pth")

evaluate_vanilla(model=model,
                 step=20,
                 batch_size=32,
                 resize=224,
                 root=PROJ_DIR / "data/celeba_subset")
