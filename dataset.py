from functools import partial
from pathlib import Path
import psutil

import torch
import os
import PIL
from typing import Any, \
    Callable, \
    List, \
    Optional, \
    Union, \
    Tuple
from torchvision.datasets import VisionDataset
from torch.utils.data import Dataset, \
    DataLoader
from torchvision.datasets.utils import download_file_from_google_drive, \
    check_integrity, \
    verify_str_arg
import numpy as np
from PIL import Image
import random

"""
This file is going to act as an abstraction for the celebA dataset and load samples from the dataset
We are using the general pytorch framework for the purpose
"""

from torch.utils.data import Dataset, \
    DataLoader
from torchvision.transforms import transforms
import os

from config import PROJ_DIR


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2 ** 32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


class AttributesDataset(Dataset):
    def __init__(self, split='train', root=os.path.join(PROJ_DIR, "data"), transform=None, dataset_min_val=0,
                 dataset_max_val=1, resize=None, adv_folder=None, groups=None,
                 bootstrap=None,
                 max_test=None):
        """
        Generate dataset object. Please set `self.min_val` and `self.max_val` .
        It would be needed for Adversarial perturbation generation.
        :param split: (string) train/valid/test
        :param transform: Transforms to be applied on model. If None, resizes image to
                (256, 256) and scales pixels to [0, 1]
        :param dataset_max_val: maximum_value of input images
        :param dataset_min_val: minimum_value of input images
        """
        self.adv_folder = adv_folder
        if transform is None and resize is None:
            transform = transforms.Compose([
                transforms.Resize((256, 256)),
                transforms.ToTensor()
            ])
        elif transform is None:
            transform = transforms.Compose([
                transforms.Resize((resize, resize)),
                transforms.ToTensor()
            ])
        self.groups = groups
        super(AttributesDataset, self).__init__()
        self.data = CelebA(
                root=root,
                split=split,
                target_type='attr',
                download=True,
                transform=transform,
                adv_folder=self.adv_folder,
                bootstrap=bootstrap,
                max_test=max_test,
        )
        # Range of values for the input
        self.min_val = dataset_min_val
        self.max_val = dataset_max_val

    def __len__(self):
        """
        Total number of samples in the dataset
        :return: Integer value representing the total number of samples
        """
        return len(self.data)

    def __getitem__(self, item):
        """
        Return a single instance of the dataset object
        :param item: index from dataset
        :return: image_index, transformed image, gt_label
        """
        if self.adv_folder:
            img, adv_imgs, label = self.data[item]
            return item, img, adv_imgs, label
        img, label = self.data[item]
        if self.groups is not None:
            label = [label[idx] for idx in self.groups]
            if len(self.groups) == 1 and len(self.groups[0]) == 1:
                return item, img, label[0]
        return item, img, label

    @staticmethod
    def pred_acc(prediction, gt_label):
        """
        Each of the datasets defines their own criterion for accuracy
        :param prediction: (B, 40) tensor of logits
        :param gt_label: (B, 40) ground truth tensor
        :return: accuracy value
        """
        return (prediction >= 0).eq(gt_label).sum().item() / 40


def get_data_loader(batch_size, split, transform=None, shuffle=False, num_workers=0, dataset_min_val=0,
                    dataset_max_val=1, resize=None, root=os.path.join(PROJ_DIR, "data"), adv_folder=None,
                    bootstrap=None,
                    max_test=None):
    """
    Return the dataloader object. Shared method for all train/val/test splits
    :param dataset_max_val: maximum_value of input images
    :param dataset_min_val: minimum_value of input images
    :param batch_size: int
    :param split: train/valid/test
    :param transform: Transform object. Default: None
    :param shuffle: boolean. Default: False
    :param num_workers: int. number of cores. Default 4
    :return: Dataloader object
    """
    return DataLoader(dataset=AttributesDataset(split=split, transform=transform, dataset_min_val=dataset_min_val,
                                                dataset_max_val=dataset_max_val, resize=resize, root=root,
                                                adv_folder=adv_folder,
                                                bootstrap=bootstrap,
                                                max_test=max_test),
                      num_workers=num_workers,
                      shuffle=shuffle,
                      batch_size=batch_size,
                      worker_init_fn=seed_worker
                      )


def check_model_size(model):
    """
    Utility function to check the number of parameters in the model. Prints the value in Millions
    :param model: Input model to be checked for size
    :return: None
    """
    num_params = 0
    traininable_param = 0
    for param in model.parameters():
        num_params += param.numel()
        if param.requires_grad:
            traininable_param += param.numel()
    print("[Network  Total number of parameters : %.3f M" % (num_params / 1e6))
    print(
            "[Network  Total number of trainable parameters : %.3f M"
            % (traininable_param / 1e6)
    )


class CelebA(VisionDataset):
    """`Large-scale CelebFaces Attributes (CelebA) Dataset <http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html>`_ Dataset.

    Args:
        root (string): Root directory where images are downloaded to.
        split (string): One of {'train', 'valid', 'test', 'all'}.
            Accordingly dataset is selected.
        target_type (string or list, optional): Type of target to use, ``attr``, ``identity``, ``bbox``,
            or ``landmarks``. Can also be a list to output a tuple with all specified target types.
            The targets represent:
                ``attr`` (np.array shape=(40,) dtype=int): binary (0, 1) labels for attributes
                ``identity`` (int): label for each person (data points with the same identity are the same person)
                ``bbox`` (np.array shape=(4,) dtype=int): bounding box (x, y, width, height)
                ``landmarks`` (np.array shape=(10,) dtype=int): landmark points (lefteye_x, lefteye_y, righteye_x,
                    righteye_y, nose_x, nose_y, leftmouth_x, leftmouth_y, rightmouth_x, rightmouth_y)
            Defaults to ``attr``. If empty, ``None`` will be returned as target.
        transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.ToTensor``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        download (bool, optional): If true, downloads the dataset from the internet and
            puts it in root directory. If dataset is already downloaded, it is not
            downloaded again.
    """

    base_folder = "celeba"
    # There currently does not appear to be a easy way to extract 7z in python (without introducing additional
    # dependencies). The "in-the-wild" (not aligned+cropped) images are only in 7z, so they are not available
    # right now.
    file_list = [
        # File ID                         MD5 Hash                            Filename
        ("0B7EVK8r0v71pZjFTYXZWM3FlRnM", "00d2c5bc6d35e252742224ab0c1e8fcb", "img_align_celeba.zip"),
        # ("0B7EVK8r0v71pbWNEUjJKdDQ3dGc", "b6cd7e93bc7a96c2dc33f819aa3ac651", "img_align_celeba_png.7z"),
        # ("0B7EVK8r0v71peklHb0pGdDl6R28", "b6cd7e93bc7a96c2dc33f819aa3ac651", "img_celeba.7z"),
        ("0B7EVK8r0v71pblRyaVFSWGxPY0U", "75e246fa4810816ffd6ee81facbd244c", "list_attr_celeba.txt"),
        ("1_ee_0u7vcNLOfNLegJRHmolfH5ICW-XS", "32bd1bd63d3c78cd57e08160ec5ed1e2", "identity_CelebA.txt"),
        ("0B7EVK8r0v71pbThiMVRxWXZ4dU0", "00566efa6fedff7a56946cd1c10f1c16", "list_bbox_celeba.txt"),
        ("0B7EVK8r0v71pd0FJY3Blby1HUTQ", "cc24ecafdb5b50baae59b03474781f8c", "list_landmarks_align_celeba.txt"),
        # ("0B7EVK8r0v71pTzJIdlJWdHczRlU", "063ee6ddb681f96bc9ca28c6febb9d1a", "list_landmarks_celeba.txt"),
        ("0B7EVK8r0v71pY0NSMzRuSXJEVkk", "d32c9cbf5e040fd4025c592c306e6668", "list_eval_partition.txt"),
    ]

    def __init__(
            self,
            root: str,
            split: str = "train",
            target_type: Union[List[str], str] = "attr",
            transform: Optional[Callable] = None,
            target_transform: Optional[Callable] = None,
            download: bool = False,
            adv_folder=None,
            random_folder=False,
            bootstrap=None,
            max_test=None,
    ) -> None:
        import pandas
        super(CelebA, self).__init__(root, transform=transform,
                                     target_transform=target_transform)
        self.split = split
        self.random_folder = random_folder
        if isinstance(target_type, list):
            self.target_type = target_type
        else:
            self.target_type = [target_type]

        if not self.target_type and self.target_transform is not None:
            raise RuntimeError('target_transform is specified but target_type is empty')

        if download:
            self.download()

        split_map = {
            "train": 0,
            "valid": 1,
            "test": 2,
            "all": None,
        }
        split_ = split_map[verify_str_arg(split.lower(), "split",
                                          ("train", "valid", "test", "all"))]

        fn = partial(os.path.join, self.root, self.base_folder)
        dir = Path(self.root) / Path(self.base_folder)
        filenames = dir.glob("*/*.jpg")
        filenames = set([f.name for f in filenames])
        splits = pandas.read_csv(fn("list_eval_partition.txt"), delim_whitespace=True, header=None, index_col=0)
        splits = splits[splits.index.isin(filenames)]

        identity = pandas.read_csv(fn("identity_CelebA.txt"), delim_whitespace=True, header=None, index_col=0)
        identity = identity[identity.index.isin(filenames)]
        bbox = pandas.read_csv(fn("list_bbox_celeba.txt"), delim_whitespace=True, header=1, index_col=0)
        bbox = bbox[bbox.index.isin(filenames)]
        landmarks_align = pandas.read_csv(fn("list_landmarks_align_celeba.txt"), delim_whitespace=True, header=1)
        landmarks_align = landmarks_align[landmarks_align.index.isin(filenames)]
        attr = pandas.read_csv(fn("list_attr_celeba.txt"), delim_whitespace=True, header=1)
        attr = attr[attr.index.isin(filenames)]
        mask = slice(None) if split_ is None else (splits[1] == split_)

        self.filename = splits[mask].index.values
        self.name_to_idx = None
        if bootstrap is not None:
            self.name_to_idx = {n: i for i, n in enumerate(self.filename)}
            self.filename = random.choices(self.filename, k=bootstrap)
        if max_test is not None and (split_ == 2 or split_ == 1):
            self.filename = random.sample(sorted(self.filename), k=max_test)
        self.identity = torch.as_tensor(identity[mask].values)
        self.bbox = torch.as_tensor(bbox[mask].values)
        self.landmarks_align = torch.as_tensor(landmarks_align[mask].values)
        self.attr = torch.as_tensor(attr[mask].values)
        self.attr = (self.attr + 1) // 2  # map from {-1, 1} to {0, 1}
        self.attr_names = list(attr.columns)
        self.adv_folder = adv_folder

    def _check_integrity(self) -> bool:
        dir = Path(self.root) / Path(self.base_folder)

        for (_, md5, filename) in self.file_list:
            fpath = dir / filename
            ext = fpath.suffix
            if fpath.is_file():
                continue
            else:
                if ext in ["zip", ".7z"]:
                    _fpath = dir / fpath.stem
                    if not _fpath.is_dir():
                        return False

        # Should check a hash of the images
        return os.path.isdir(os.path.join(self.root, self.base_folder, "img_align_celeba"))

    def download(self) -> None:
        import zipfile

        if self._check_integrity():
            print('Files already downloaded and verified')
            return

        for (file_id, md5, filename) in self.file_list:
            download_file_from_google_drive(file_id, os.path.join(self.root, self.base_folder), filename, md5)

        with zipfile.ZipFile(os.path.join(self.root, self.base_folder, "img_align_celeba.zip"), "r") as f:
            f.extractall(os.path.join(self.root, self.base_folder))

    def __getitem__(self, index: int) -> Tuple:
        X = PIL.Image.open(os.path.join(self.root, self.base_folder, "img_align_celeba", self.filename[index]))
        name = self.filename[index]
        if self.name_to_idx is not None:
            index = self.name_to_idx[name]
        target: Any = []
        for t in self.target_type:
            if t == "attr":
                target.append(self.attr[index, :])
            elif t == "identity":
                target.append(self.identity[index, 0])
            elif t == "bbox":
                target.append(self.bbox[index, :])
            elif t == "landmarks":
                target.append(self.landmarks_align[index, :])
            else:
                # TODO: refactor with utils.verify_str_arg
                raise ValueError("Target type \"{}\" is not recognized.".format(t))

        if self.transform is not None:
            X = self.transform(X)

        if target:
            target = tuple(target) if len(target) > 1 else target[0]

            if self.target_transform is not None:
                target = self.target_transform(target)
        else:
            target = None
        if self.adv_folder is not None:
            adv_name = name
            if isinstance(self.adv_folder, list) or isinstance(self.adv_folder, tuple):
                if self.random_folder:
                    idx = np.random.randint(len(self.adv_folder))
                    folder = self.adv_folder[idx]
                    adv_img = PIL.Image.open(Path(folder) / adv_name)
                    if self.transform is not None:
                        return X, self.transform(adv_img), target
                # else: we return all the adv_imgs
                adv_imgs = list()
                for folder in self.adv_folder:
                    adv_img = PIL.Image.open(Path(folder) / adv_name)
                    if self.transform is not None:
                        adv_img = self.transform(adv_img)
                    adv_imgs.append(adv_img)
                return X, adv_imgs, target
            # only one folder:
            adv_img = PIL.Image.open(Path(self.adv_folder) / adv_name)
            if self.transform is not None:
                adv_img = self.transform(adv_img)
            return X, adv_img, target
        return X, target

    def __len__(self) -> int:
        return len(self.filename)

    def extra_repr(self) -> str:
        lines = ["Target type: {target_type}", "Split: {split}"]
        return '\n'.join(lines).format(**self.__dict__)

class PertFolder(VisionDataset):
    def __init__(self, filename, dir, transform=None, resize=224):
        super().__init__(root=dir)

        if transform is None and resize is None:
            self.transform = transforms.Compose([
                transforms.Resize((224, 224)),
                transforms.ToTensor()
            ])
        elif transform is None:
            self.transform = transforms.Compose([
                transforms.Resize((resize, resize)),
                transforms.ToTensor()
            ])

        self.dir = Path(dir)
        if not self.dir.is_dir():
            self.dir.mkdir()
        self.filename = filename

    def __getitem__(self, index: int) -> Tuple:
        path = Path(self.dir) / "{}".format(self.filename[index])
        X = PIL.Image.open(path)
        if self.transform is not None:
            X = self.transform(X)
        return X

    def __len__(self):
        return len(self.filename)


def create_attribute_dataloader(batch_size, resize=None, root=PROJ_DIR / "data", 
                                adv_folder=None, 
                                train_transform=None,
                                test_transform=None,
                                bootstrap=None,
                                max_test=None):
    num_cpu = psutil.cpu_count(logical=False)  # multiprocessing only uses physical cores
    num_cpu = max(1, num_cpu - 2)  # not using all processes available
    print("Using {} cpus for dataloading".format(num_cpu))
    dataloader_train = get_data_loader(batch_size, split='train',
                                       transform=train_transform, shuffle=True, num_workers=num_cpu,
                                       dataset_min_val=0, dataset_max_val=1, resize=resize, root=root,
                                       adv_folder=adv_folder,
                                       bootstrap=bootstrap)
    dataloader_val = get_data_loader(batch_size, split='valid',
                                     transform=test_transform, shuffle=False, num_workers=num_cpu,
                                     dataset_min_val=0, dataset_max_val=1, resize=resize, root=root,
                                     adv_folder=adv_folder,
                                     max_test=max_test)
    dataloader_test = get_data_loader(batch_size, split='test',
                                      transform=test_transform, shuffle=False, num_workers=num_cpu,
                                      dataset_min_val=0, dataset_max_val=1, resize=resize, root=root,
                                      adv_folder=adv_folder,
                                      max_test=max_test)
    return dataloader_train, dataloader_val, dataloader_test

# loaders = create_attribute_dataloader(12,224,root=PROJ_DIR / "data/celeba_subset",adv=True)

# for batch in loaders[0]:
#     indexes, imgs, adv_imgs, labels = batch
#     print(imgs.max())
#     print(adv_imgs.max())
#     break
