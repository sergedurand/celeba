from models.simple_net import Net
from trainer_adv import CelebA_adv
from utils import check_model_size
from utils import changeNumClass
import timm
from torchvision import models
import torch
from dataset import get_data_loader

# names = ["vgg11","vgg16","vgg16","vgg19","tv_resnet50","tv_resnext50_32x4d","tv_resnet101","seresnext50_32x4d",
# "resnext50d_32x4d","resnext101_32x8d","resnet50","resnet200d","wide_resnet101_2","tf_efficientnet_b6","tf_efficientnet_b3"]

for name in timm.list_models(pretrained=True):
    if  "ecaresnet" in str(name):
        m = timm.create_model(name,pretrained=False,num_classes=40)
        print(name)
        check_model_size(m)

# names = ["swsl_resnext101_32x16d","ssl_resnext101_32x16d","skresnext50_32x4d","rexnet_200","ig_resnext101_32x16d","inception_resnet_v2","inception_v4"]

# for name in names:
#     model = timm.create_model(name,pretrained=True)
#     model = changeNumClass(model,40)


# model = timm.create_model("tf_efficientnet_l2_ns_475", num_classes=40,pretrained=True)
# check_model_size(model)
# print(model.getattr("classifier"))



