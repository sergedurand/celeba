
# Face attribute defense
## Step 1: Transfer learning
In a first step we evaluate common models trained for classification on ImageNet. The setting is:
- Dataset: we use a subset of celebA, randomly chosen (the subset is the same for every network, we do the random selection only once): 
Size train = 16071
Size val = 1919
Size test = 2010
- Transfer: we change the classifier of the model to predict 40 classes, (done by timm when possible or by hand if not (only the case for googlenet))
- Training:
    - we use an Adam optimizer with default parameters: https://pytorch.org/docs/master/generated/torch.optim.Adam.html
    - we use early stopping monitoring for validation accuracy, a patience of 15 and a min delta of 1e-4 (i.e we stop if the change on validation of accuracy was less than 1e-4 for 15 checks in a row)
    - We record validation accuracy 5 times per epoch
    - Batch size = 4 or 2 if 4 failed (memory issue for some models e.g efficientnet b8)
    - accumulating grad batch factor = 4 (see https://pytorch-lightning.readthedocs.io/en/stable/advanced/training_tricks.html) so in effect the batch size is 16 or 8
    - We retrain all parameters, we don't freeze anything (freezing helped train faster, but decreased accuracy)
    
Baseline architecture is:
```python
self.conv1 = nn.Conv2d(3, 6, 5)
self.pool = nn.MaxPool2d(2, 2)
self.conv2 = nn.Conv2d(6, 16, 5)
self.fc1 = nn.Linear(out_shape, 120)
self.fc2 = nn.Linear(120, 84)
self.fc3 = nn.Linear(84, num_classes)
```

results :

|Model | Final test acc (%) | Total training time | Time / epoch | Size (MB)|
|--- | --- | --- | --- | ---|
|baseline | 87,4 | 2min32 | 25 s | 21|
|Efficientnet_b8 | 89,89 | 1h30 | 12 min | 326|
|resnet50 | 89,97 | 21min50 | 3 min | 91|
|vit | 87,56 | 1h35 | 6 min | 328|
|wide_resnet_101 | 88,84 | 2h10 | 9 min | 478|
|resnet200d | 90,16 | 42min | 8 min | 241|
|resnext101 | 89,85 | 2h10 | 11 min | 333|
|googlenet | 90,31 | 14min | 1min30 | 21|
|tf_efficientnet_b7_ns | 89,78 | 1h 11 | 10 min | 246|
|tf_efficientnet_b7_ap | 90,14 | 42min | 8 min | |
|adv_inception_v3 | 89,86 | 22 min | 3 min | |


resnet50 and googlenet seems to be good enough to iterate on ideas in addition to the baseline simple net.


## Step 2 Transferred attack

We try to evaluate with common attacks and transferred attacks.
The transfered attacks are PGD 20 computed on a resnext101 trained on all the dataset celebA (test accuracy = 91.36) and on the baseline.

Results: 

|Model name | clean | FGSM | BIM | PGD | PGD transferred | Delta contest | Delta transferred|
|--- | --- | --- | --- | --- | --- | --- | ---|
|resnet50_subset,pth | 89,96 | 74,55 | 53,50 | 53,92 | 74,67 | 23,79 | 15,30|
|tf_efficientnet_b7_ap_subset,pth | 90,23 | 75,49 | 51,70 | 49,60 | 69,03 | 26,84 | 21,20|
|wide_resnet101_2_subset,pth | 88,84 | 74,42 | 59,37 | 59,93 | 73,18 | 20,94 | 15,66|
|baseline_subset,pth | 87,40 | 71,61 | 61,63 | 62,88 | 72,68 | 19,35 | 14,72|
|googlenet_subset,pth | 90,44 | 77,23 | 63,95 | 64,91 | 76,56 | 18,79 | 13,88|
|adv_inception_v3_subset,pth | 90,41 | 77,62 | 63,30 | 63,44 | 74,14 | 19,91 | 16,27|
|tf_efficientnet_b8_subset,pth | 89,89 | 75,01 | 50,74 | 48,86 | 76,85 | 23,85 | 13,03|
|resnet200d_subset,pth | 90,16 | 73,23 | 55,48 | 55,18 | 76,36 | 22,78 | 13,80|
|vit_base_patch16_224_subset,pth | 87,56 | 71,94 | 52,83 | 54,23 | 75,78 | 21,72 | 11,77|

    
Delta contest is 0.2 delta FGSM + 0.4 delta BIM + 0.4 delta PGD.
We use 20 iteration for BIM and PGD.

- FGSM < PGD ~= BIM
- PGD transferred on par with FGSM
- Googlenet most resilient without any kind of defense. Baseline close second. Simpler model = more resistant?
- At best we seem to lower accuracy to random accuracy, not so much below. To go lower probably need to
consider the task at hand, built attacks on specific attributes that will "sink" performance
  
- A quick try of PGD with 1000 steps on the baseline led to 61.15 % accuracy, not much gain from increasing steps.



## Step 3 adversarial training

We try three ways to do adversarial training: 
- 1) using transferred attacks from the resnext101 model, 
- 2) using pre-computed attacks on a clean version of the model that we train for, 
- 3) computing adversarial images on the model as we go.

The first 2 have the advantage to have the same cost as normal training. However the models trained this way are not really more robust (although they can be more accurate).

The last is quite costly, but does increase robustness, at the cost of accuracy, as expected.

For instance the final delta (see competition rules: 0.2 * fgsm delta + 0.4 * BIM delta + 0.4 * PGD delta) is 2.38 for PGD trained, 22.11 for PGD transferred and 22.41 for normal baseline.

The setting is the same as clean training (early stopping, adam optim etc).

During training we chose the perturbed image or the clean image with 50% chance, so the network is trained on both clean and perturbed images.

Results:

|Model name | clean | FGSM | BIM | PGD | SMIA_0 | SMIA_1 | PGD transferred|
|--- | --- | --- | --- | --- | --- | --- | ---|
|baseline_subset_static_SMIA_20_A2_0,pth | 87,49 | 72,65 | 63,57 | 59,60 | 59,19 | 74,61 | 82,46|
|baseline_subset_dynamic_SMIA_20_A2_1,pth | 28,85 | 28,75 | 28,69 | 28,20 | 29,25 | 28,92 | 28,37|
|baseline_subset_dynamic_PGD_20,pth | 85,47 | 83,27 | 82,99 | 73,74 | 67,58 | 79,53 | 83,72|
|baseline_subset_dynamic_SMIA_20_a2_0,pth | 80,02 | 80,02 | 80,02 | 79,99 | 79,98 | 79,99 | 79,99|
|baseline_subset_static_PGD_20,pth | 87,52 | 71,14 | 62,27 | 59,24 | 58,86 | 73,90 | 79,32|
|baseline_subset_adv_PGD_20_transferred_all_data_resnext101,pth | 87,39 | 71,30 | 61,97 | 59,75 | 58,62 | 73,73 | 89,24|
|baseline_subset_static_SMIA_20_A2_1,pth | 87,54 | 75,34 | 65,49 | 59,42 | 56,58 | 72,33 | 82,62|


## Step 4 new attack: SMIA
*UPDATE: The original implementation lead to adversarial attacks way out of the 8/255 ball, which then lead networks using them to train to predict the same
output regardless of input (which still had around 80% accuracy). I need to properly rerun stuff but it seems that SMIA is 
not really better than PGD as an attack, not sure for defense. It still seems that the stabilization is not good for our task,
the attack is worse.*


We try the Stabilized Medical Image Attack (https://github.com/imogenqi/SMA/blob/main/SMIA.py, https://openreview.net/pdf?id=QfTXQiGYudJ). 

The stabilization term does not seem useful for us. The attacks are not as performant as PGD or BIM or even FGSM.

However the attack used with no stabilization loss (a2 = 0 in the code), leads to better attacks (slightly) than PGD (both with 20 iterations).

|Model name | clean | FGSM | BIM | PGD | SMIA_0 | SMIA_1 | SMIA_02 | SMIA_04 | SMIA_06 | SMIA_08 | PGD transferred|
|--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---|
|baseline_subset,pth | 87,33 | 71,69 | 61,76 | 63,02 | 65,93 | 82,78 | 82,00 | 82,46 | 82,63 | 82,73 | 72,34|

(SMIA_0x means SMIA attack with a2 = x)

Surprisingly, when used for adversarial training it leads the networks (baseline, googlenet, resnet50 or tf_effnet_b7_ap) to learn to predict a single output regardless of input, that still has 80% accuracy.

Results:



After further investigation it seems that this also happens with PGD using larger epsilon.
It seems that SMIA is just BIM with larger epsilon (visualization also seems to lead to this conclusion).
However the perturbed image is not penalized by the toolkit??

## Step 5 Investigate Ensembling

We try to use an ensemble of trained models. The intuition is that it's harder to fool several model rather than one.
First we use model trained on the subset on clean images only.
We evaluate of the first 1000 images from the subset of the test set.
We use PGD attack with 20 steps.

Results :
|Model | clean | PGD adv|
|--- | --- | ---|
|baseline_subset | 87,32 | 62,99|
|googlenet_subset | 90,35 | 66,85|
|resnet50_subset | 89,96 | 54,16|
|tf_efficientnet_b3_ap_subset | 88,55 | 60,02|
|ecaresnet50d_pruned | 90,53 | 53,63|
|Average | 89,34 | 59,53|
|Ensemble vote | 90,82 | 90,57|
|Ensemble average | 90,95 | 60,25|

- Ensemble by voting or averaging logits gives better clean accuracy than the best of the model component,
or the average accuracy of components
  
- The attack fails on the ensemble with voting: the vote is a non-differentiable operation, this amounts to a 
defense with gradients obfuscation.
  
- The ensemble with averaging is as sensitive to PGD attacks as its component.

We then try the same ensemble model but with transferred attack to avoid the problem of computing gradients.
We still use an averaging model where we set every positive logits to 10 after averaging, and -10 for the 
negative, making it robust to gradient based attack.

We use each component model to produce the transferred attack. Note that in the grey-box setting the attacker 
can't do that as it doesn't know the model component (as far as I know).

Result for the averaging ensemble:

|Attacker | Adv acc | delta|
|--- | --- | ---|
|baseline_subset | 76,99 | 13,95|
|googlenet_subset | 82,32 | 8,63|
|resnet50_subset | 75,37 | 15,58|
|tf_efficientnet_b3_ap_subset | 87,46 | 3,49|
|ecaresnet50d_pruned | 70,36 | 20,59|

For the voting ensemble:

|Attacker | Adv acc | delta|
|--- | --- | ---|
|baseline_subset | 85,12 | 5,70|
|googlenet_subset | 82,41 | 8,41|
|resnet50_subset | 81,10 | 9,72|
|tf_efficientnet_b3_ap_subset | 88,87 | 1,95|
|ecaresnet50d_pruned | 82,14 | 8,68|


- Both ensembling mitigate from transferred attack to some extent.
- Voting seems to be around twice as good as a defense (notable exception: the googlenet attack)
- Note that the efficientnet makes a very poor attacker. This might be because it's a model originally trained on
adversarial samples from imagenet (https://arxiv.org/abs/1911.09665)

## What's next:

- Use fancier networks specialized in facial attributes recognition, like those using multi-task learning, or networks
using a backbone and different classifiers for specific groups of features (how to group them?).
- Try adversarial learning from ensemble of attacks (https://openreview.net/pdf?id=rkZvSe-RZ)
- Test the autoencoder in the toolkit
- Test denoising defense (https://arxiv.org/pdf/2012.09384.pdf, https://arxiv.org/abs/1812.03411)
- Test transferred attack on obfuscated clean model
- Test transferred attack on obfuscated adv trained model
- Same as above two on ensemble model
- Try more standard attacks (DeepFool ...), adapting https://adversarial-attacks-pytorch.readthedocs.io/en/latest/attacks.html
- Try some attacks on subset of attributes, not the 40 at once. Evaluate the cross performance:
  (create adversarial sample for each attribute, and check accuracy decrease for each of the attributes, 40 x 40)
  
- Find groups of attributes used in the literature to improve classification, evaluate attacks targeting each groups
- Test CW attack
- Add randomization defense (https://openreview.net/pdf?id=Sk9yuql0Z and many other). 
to make the model deterministic, find a way to compute a unique seed based on the input image
### to explore:
- https://www.ijcai.org/proceedings/2018/0091.pdf
- https://arxiv.org/pdf/2007.11709.pdf
- https://arxiv.org/pdf/1806.00194.pdf
- https://openaccess.thecvf.com/content_CVPR_2020/papers/Wang_Towards_Fairness_in_Visual_Recognition_Effective_Strategies_for_Bias_Mitigation_CVPR_2020_paper.pdf
- https://arxiv.org/pdf/2011.09824.pdf
- https://arxiv.org/abs/1904.08492
- https://github.com/espectre/Facial_Attributes_MTL_Basiline
- https://arxiv.org/pdf/1805.01290v1.pdf
- https://arxiv.org/pdf/1808.01753.pdf (gray box training)
- http://myweb.sabanciuniv.edu/berrin/files/2018/12/icme-final-paper.pdf (grouping attributes)
- https://discovery.ucl.ac.uk/id/eprint/10091120/1/TAFFC-LongbiaoMao-YanYan-R2.pdf 
  (top 3 state of the art for celebA, see section 2.2 for gouping attributes)
  
- https://openaccess.thecvf.com/content_cvpr_2018/papers/Cao_Partially_Shared_Multi-Task_CVPR_2018_paper.pdf 
  (state of the art for celebA)
  
- https://www.arxiv-vanity.com/papers/1904.02920/
- https://arxiv.org/pdf/1911.07846.pdf
- https://arxiv.org/abs/2005.13712 obfuscating gradient defense
## TODO
- Clean logs and share tensorboard.dev links
