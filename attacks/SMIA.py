import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import math
from torch.autograd import Variable
from torch import cuda
import scipy.stats as st

from attacks.abstract_attacks import AbstractAttack


def gkern(kernlen=3, nsig=1):
    """Returns a 2D Gaussian kernel array."""
    x = np.linspace(-nsig, nsig, kernlen)
    kern1d = st.norm.pdf(-1, x)
    kern2d = st.norm.pdf(-1, x)
    kernel_raw = np.outer(kern1d, kern2d)
    kernel = kernel_raw / kernel_raw.sum()
    return kernel


def blur(tensor_image, epsilon, stack_kerne):
    min_batch = tensor_image.shape[0]
    channels = tensor_image.shape[1]
    out_channel = channels
    kernel = torch.FloatTensor(stack_kerne).cuda()
    weight = nn.Parameter(data=kernel, requires_grad=False)
    data_grad = F.conv2d(tensor_image, weight, bias=None, stride=1, padding=(2, 0), dilation=2)

    sign_data_grad = data_grad.sign()

    perturbed_image = tensor_image + epsilon * sign_data_grad
    return data_grad * epsilon


class SMIA(AbstractAttack):
    def __init__(self, model,loss_fn,save_folder=None, epsilon=8/255, a1=1,a2=0, num_iterations=40, targeted=False,
                 min_val=0, max_val=1):
        """
        One step fast gradient sign method
        """
        super().__init__(model=model, save_folder=save_folder, min_val=min_val, max_val=max_val)
        self.epsilon = epsilon
        self.loss_fn = loss_fn
        self.loss_img = nn.MSELoss()
        self.step = num_iterations
        self.a1 = a1
        self.a2 = a2

    def attack(self, images, labels, epsilons=None):
        """
        Given examples (X, y), returns their adversarial
        counterparts with an attack length of epsilon.
        """
        # Providing epsilons in batch
        if epsilons is not None:
            self.epsilon = epsilons

        use_cuda = torch.cuda.is_available()
        FloatTensor = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor
        if isinstance(self.loss_fn, torch.nn.BCEWithLogitsLoss):
            # Converting labels into float as required for BCELoss
            labels = labels.to(torch.float)

        X_pert = Variable((images.clone().detach())).cuda()
        X_pert.requires_grad = True

        for i in range(self.step):
            with torch.enable_grad():
                output_perturbed = self.model(X_pert.cuda())
                if i == 0:
                    loss = self.loss_fn(output_perturbed, labels)
                else:
                    loss = self.a1 * self.loss_fn(output_perturbed, labels) - self.a2 * self.loss_fn(output_perturbed,
                                                                                                     output_perturbed_last)
                loss.backward()
            with torch.no_grad():
                X_pert_grad = X_pert.grad.detach().sign()
                pert = X_pert.grad.detach().sign() * (self.epsilon / self.step)

                kernel = gkern(3, 1).astype(np.float32)
                stack_kernel = np.stack([kernel, kernel, kernel]).swapaxes(2, 0)
                stack_kernel = np.expand_dims(stack_kernel, 3)

                gt1 = X_pert_grad.detach()
                gt1 = blur(gt1, self.epsilon, stack_kernel)
                gt1 = X_pert + gt1
                gt1 = Variable(gt1.clone().detach()).cuda()
                gt1.requires_grad_(False)
                output_perturbed_last = self.model(gt1)
                # _, output_perturbed_last = torch.max(output_perturbed_last, dim=1)

                X_pert = torch.clamp(Variable(X_pert).cuda() + pert.cuda(), min=self.min_val, max=self.max_val)
            X_pert.requires_grad = True

        return X_pert









