from torch import nn
import torch.nn.functional as F
import torch

def create_conv_block(in_channel, out_channel, kernel_size, stride=2, dropout_p=0):
    """
    Convolution Block
    :param stride: stride applied to the conv kernel
    :param in_channel: Number of input channels to the conv kernel
    :param out_channel: Number of output channels of conv kernel
    :param kernel_size: size of the kernel applied
    :param dropout_p: dropout probability used
    :return: Sequential object containing Conv->ReLU->Dropout
    """
    return nn.Sequential(
        nn.Conv2d(in_channels=in_channel, out_channels=out_channel, kernel_size=kernel_size, stride=stride),
        nn.ReLU(),
        nn.Dropout(dropout_p)
    )


def create_trasnpose_conv_block(in_channel, out_channel, kernel_size, stride=2, dropout_p=0):
    """
    Transpose Convolution Block
    :param in_channel: Number of input channels to the TransposeConv kernel
    :param out_channel: Number of output channels of TransposeConv kernel
    :param kernel_size: size of the kernel applied
    :param stride: stride applied to the TransposeConv kernel
    :param dropout_p: dropout probability used
    :return: Sequential object containing TransposeConv->ReLU->Dropout
    """
    return nn.Sequential(
        nn.ConvTranspose2d(in_channels=in_channel, out_channels=out_channel, kernel_size=kernel_size, stride=stride),
        nn.ReLU(),
        nn.Dropout(dropout_p)
    )


class AutoEncoderDefenceNetwork(nn.Module):
    def __init__(self,input_shape=224, num_classes=40):
        super().__init__()
        self.num_classes = num_classes
        self.additional_loss = 0
        aut_enc_down1 = create_conv_block(in_channel=3, out_channel=64, kernel_size=3, stride=2)
        aut_enc_down2 = create_conv_block(in_channel=64, out_channel=128, kernel_size=3, stride=2)
        aut_enc_down3 = create_conv_block(in_channel=128, out_channel=256, kernel_size=3, stride=2)
        aut_enc_down4 = create_conv_block(in_channel=256, out_channel=512, kernel_size=3, stride=2)
        # Upsampling layers
        aut_enc_up1 = create_trasnpose_conv_block(in_channel=512, out_channel=256, kernel_size=3, stride=2)
        aut_enc_up2 = create_trasnpose_conv_block(in_channel=256, out_channel=128, kernel_size=3, stride=2)
        aut_enc_up3 = create_trasnpose_conv_block(in_channel=128, out_channel=64, kernel_size=3, stride=2)
        aut_enc_up4 = nn.ConvTranspose2d(in_channels=64, out_channels=3, kernel_size=3, stride=2, output_padding=1)
        self.enc_dec = nn.Sequential(
            aut_enc_down1, aut_enc_down2, aut_enc_down3, aut_enc_down4,
            aut_enc_up1, aut_enc_up2, aut_enc_up3, aut_enc_up4
        )
        # Normal Conv layers in order to get the final classification
        self.ConvLayer1 = create_conv_block(in_channel=3, out_channel=64, kernel_size=3, stride=2)
        self.ConvLayer2 = create_conv_block(in_channel=64, out_channel=128, kernel_size=3, stride=2)
        self.ConvLayer3 = create_conv_block(in_channel=128, out_channel=256, kernel_size=3, stride=2)
        self.ConvLayer4 = create_conv_block(in_channel=256, out_channel=512, kernel_size=3, stride=2)
        x_hat = torch.rand(1,3,input_shape,input_shape)
        x_hat = self.enc_dec(x_hat)
        x_hat = self.ConvLayer1(x_hat)
        x_hat = self.ConvLayer2(x_hat)
        x_hat = self.ConvLayer3(x_hat)
        x_hat = self.ConvLayer4(x_hat)
        kernel_size = x_hat.shape[-1]
        self.Linear1 = nn.Conv2d(in_channels=512, out_channels=num_classes, kernel_size=kernel_size)

    def forward(self, noisy_images, actual_images=None):
        # Pass through the Denoising Autoencoder setting first
        x_hat = self.enc_dec(noisy_images)
        # In training time, we have the original images present which we can denoise and learn upon
        if actual_images is not None:
            self.additional_loss = F.mse_loss(x_hat, actual_images)
        else:
            self.additional_loss = 0
        x_hat = self.ConvLayer1(x_hat)
        x_hat = self.ConvLayer2(x_hat)
        x_hat = self.ConvLayer3(x_hat)
        x_hat = self.ConvLayer4(x_hat)
        x_hat = self.Linear1(x_hat)
        x_hat = x_hat.view(x_hat.shape[0], -1)  # from B, C, 1, 1 -> B, C
        return x_hat