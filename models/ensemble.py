import torch
from torch import nn
import random

class Ensemble(nn.Module):
    def __init__(self,models, mode="vote"):
        super().__init__()
        self.backbone = nn.ModuleList(models)
        self.mode = mode

    def vote(self,scores):
        scores[scores > 0] = 1
        scores[scores <= 0] = -1
        scores = torch.sum(scores,dim=0)
        scores[scores > 0] = 10
        scores[scores <= 0] = -10
        return scores

    def average(self,scores):
        scores = scores.mean(dim=0)
        scores[scores > 0] = 10
        scores[scores <= 0] = -10
        return scores

    def random(self, scores):
        idx = random.choice(range(len(self.backbone)))
        res = scores[idx]
        res[res>0] = 10
        res[res<=0] = -10
        return scores[idx]

    def forward(self, x):
        logits = list()
        for model in self.backbone:
            _logits = model(x)
            if isinstance(_logits, tuple):
                _logits = _logits[0]
            logits.append(_logits)
        logits = torch.stack(logits)
        if self.mode == "vote":
            return self.vote(logits)
        elif self.mode == "average":
            return self.average(logits)
        elif self.mode == "random":
            return self.random(logits)