import torch.nn as nn
import torch.nn.functional as F
import torch
from numbers import Number

class Net(nn.Module):
    def __init__(self,num_classes=40,shape=224):
        super().__init__()
        if isinstance(shape, Number):
            shape1 = shape
            shape2 = shape
        else:
            shape1, shape2 = shape
        x = torch.rand(1,3,shape1,shape2)
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        with torch.no_grad():
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = torch.flatten(x,1)
        out_shape = x.shape[-1]
        self.fc1 = nn.Linear(out_shape, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, num_classes)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        features = x
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


class NetFeat(nn.Module):
    def __init__(self,num_classes=40,shape=224):
        super().__init__()
        if isinstance(shape, Number):
            shape1 = shape
            shape2 = shape
        else:
            shape1, shape2 = shape
        x = torch.rand(1,3,shape1,shape2)
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        with torch.no_grad():
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = torch.flatten(x,1)
        out_shape = x.shape[-1]
        self.fc1 = nn.Linear(out_shape, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, num_classes)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        features = x
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x, features

net = Net()