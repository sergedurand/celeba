import torch
import torch.functional as F

class Resnet(torch.nn.Module):
    def __init__(self, backbone):
        super().__init__()
        self.backbone = backbone

    def forward(self, x):
        x = self.backbone.forward_features(x)
        features = x
        x = self.backbone.global_pool(x)
        if self.backbone.drop_rate:
            x = F.dropout(x, p=float(self.backbone.drop_rate), training=self.training)
        x = self.backbone.fc(x)
        return x, features