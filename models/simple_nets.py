import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.parent))
import torch.nn as nn
import torch.nn.functional as F
import torch
from numbers import Number
from utils import group_loc

class Net(nn.Module):
    def __init__(self,num_classes=40,shape=224):
        super().__init__()
        if isinstance(shape, Number):
            shape1 = shape
            shape2 = shape
        else:
            shape1, shape2 = shape
        x = torch.rand(1,3,shape1,shape2)
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        with torch.no_grad():
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = torch.flatten(x,1)
        out_shape = x.shape[-1]
        self.fc1 = nn.Linear(out_shape, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, num_classes)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        features = x
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


net = Net()


class Autoencoder(nn.Module):
    def __init__(self,num_classes=40,shape=224):
        super().__init__()
        if isinstance(shape, Number):
            shape1 = shape
            shape2 = shape
        else:
            shape1, shape2 = shape
        x = torch.rand(1,3,shape1,shape2)
        self.encoder = nn.Sequential(
            nn.Conv2d(3, 16, 3, stride=3, padding=1),  # b, 16, 10, 10
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=2),  # b, 16, 5, 5
            nn.Conv2d(16, 8, 3, stride=2, padding=1),  # b, 8, 3, 3
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=1)  # b, 8, 2, 2
        )
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(8, 16, 3, stride=2),  # b, 16, 5, 5
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 8, 5, stride=3, padding=1),  # b, 8, 15, 15
            nn.ReLU(True),
            nn.ConvTranspose2d(8, 3, 2, stride=2, padding=1),  # b, 1, 28, 28
            nn.Tanh()
        )
        with torch.no_grad():
            y = self.decoder(self.encoder(x))
        shape = (y.shape[-2],y.shape[-1])
        self.classifier = Net(num_classes=num_classes,shape=shape)
        

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        x = self.classifier(x)
        return x

net = Autoencoder()
x = torch.rand(100,3,224,224)
y = net(x)

class SharedNet(nn.Module):
    def __init__(self,num_classes=40,shape=224,groups=None):
        super().__init__()
        if isinstance(shape, Number):
            shape1 = shape
            shape2 = shape
        else:
            shape1, shape2 = shape
        x = torch.rand(1,3,shape1,shape2)
        if groups is None:
            groups = [list(range(40))]
        models = list()
        self.groups = groups
        self.num_classes = num_classes
        for group in groups:
            _num_classes = len(group)
            model = Net(num_classes=_num_classes,shape=shape)
            models.append(model)

        models = nn.ModuleList(models)
        self.models = models
        features = list()
        for model in models:
            _, feature = model(x)
            features.append(feature)
        total_feature = torch.stack(features)
        batch_size = total_feature.shape[1]
        total_feature = total_feature.view(batch_size,-1)
        self.classifier = nn.Linear(total_feature.shape[-1],num_classes)
        
    def forward(self, x):
        preds = list()
        features = list()
        for model in self.models:
            _preds, _features = model(x)
            preds.append(_preds)
            features.append(_features)
        features = torch.stack(features)
        features = features.view(x.shape[0],-1)
        return preds, self.classifier(features)

# net = SharedNet(40,224,group_loc())
# preds, x = net(torch.rand(100,3,224,224))
# print(len(preds))
# print(x.shape)
# for pred in preds:
#     print(pred.shape)