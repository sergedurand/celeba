"""
inspired by https://colab.research.google.com/github/PytorchLightning/pytorch-lightning/blob/master/notebooks/01-mnist-hello-world.ipynb#scrollTo=4DNItffri95Q
"""
import sys
from config import PROJ_DIR
import pytorch_lightning as pl
from pytorch_lightning.metrics.functional import accuracy
from torch import nn
import torch
from torch.utils.data import Subset
import random

from dataset import create_attribute_dataloader


class CelebAModel(pl.LightningModule):
    def __init__(self, model, loss_fn=nn.BCEWithLogitsLoss(), freeze=False,
                 batch_size=32, resize=None, root=PROJ_DIR / "data", lr=1e-3, sgd=False,
                 train_transform=None,
                 test_transform=None,
                 bootstrap=None,
                 max_test=None):
        super().__init__()
        self.model = model
        self.classifier = [n for n, _ in self.model.named_children()][-1]
        # print("Classifier is {}, type : {}".format(self.classifier,type(list(self.model.named_children())[-1][-1]).__name__))
        self.loss_fn = loss_fn
        self.freeze = freeze
        self.batch_size = batch_size
        self.resize = resize
        self.loaders = create_attribute_dataloader(self.batch_size, resize=resize, root=root,
                                                   train_transform=train_transform,
                                                   test_transform=test_transform,
                                                   bootstrap=bootstrap)
        if max_test is not None:
            n = len(self.loaders[1].dataset)
            new_idx = random.sample(range(n),k=max_test)
            self.loaders[1].dataset.data = Subset(self.loaders[1].dataset.data,new_idx)
            n = len(self.loaders[2].dataset)
            new_idx = random.sample(range(n), k=max_test)
            self.loaders[2].dataset.data = Subset(self.loaders[2].dataset.data, new_idx)
        self.size = len(self.train_dataloader().dataset)
        self.lr = lr
        self.sgd = sgd
        self.save_hyperparameters("batch_size", "loss_fn", "freeze", "resize", "root", "lr", "sgd")

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_nb):
        _, imgs, labels = batch
        if isinstance(self.loss_fn, nn.BCEWithLogitsLoss) or isinstance(self.loss_fn, nn.BCELoss):
            labels = labels.to(torch.float)
        logits = self(imgs)
        if isinstance(logits, tuple):
            logits = logits[0]
            teacher_labels = logits[1].clone()
            teacher_labels[teacher_labels>0] = 1
            teacher_labels[teacher_labels<=0] = 0
            loss1 = self.loss_fn(logits[0], teacher_labels)
            loss2 = self.loss_fn(logits[1], labels)
            loss = 0.5 * loss1 + 0.5 * loss2
            return loss
        loss = self.loss_fn(logits, labels)
        return loss

    def validation_step(self, batch, batch_idx):
        _, imgs, labels = batch
        if isinstance(self.loss_fn, nn.BCEWithLogitsLoss) or isinstance(self.loss_fn, nn.BCELoss):
            labels = labels.to(torch.float)
        logits = self(imgs)
        if isinstance(logits, tuple):
            logits = logits[0]
        loss = self.loss_fn(logits, labels)
        acc = (logits > 0).eq(labels).sum().item() / 40
        acc = acc / imgs.shape[0]
        self.log('val_loss', loss, prog_bar=True)
        self.log('val_acc', acc, prog_bar=True)
        return loss, acc

    def test_step(self, batch, batch_idx):
        # Here we just reuse the validation_step for testing
        _, imgs, labels = batch
        if isinstance(self.loss_fn, nn.BCEWithLogitsLoss) or isinstance(self.loss_fn, nn.BCELoss):
            labels = labels.to(torch.float)
        logits = self(imgs)
        if isinstance(logits, tuple):
            logits = logits[0]
        loss = self.loss_fn(logits, labels)
        acc = (logits > 0).eq(labels).sum().item() / 40
        acc = acc / imgs.shape[0]
        self.log('test_loss', loss, prog_bar=True)
        self.log('test_acc', acc, prog_bar=True)
        return loss, acc

    def configure_optimizers(self):
        if self.freeze:
            for name, params in self.model.named_modules():
                if name in ["fc", "classif", "head", "classifier", "last_linear", "conv_head"]:
                    for _params in params.parameters():
                        _params.requires_grad = True
                    continue
                for _name, trained_params in params.named_parameters():
                    trained_params.requires_grad = False
            print("Only optimizing classifier")
            if self.classifier == "fc":
                return torch.optim.AdamW(self.model.fc.parameters()) if not self.sgd else torch.optim.SGD(
                    self.model.fc.parameters(), lr=self.lr)
            elif self.classifier == "head":
                return torch.optim.AdamW(self.model.head.parameters()) if not self.sgd else torch.optim.SGD(
                    self.model.head.parameters(), lr=self.lr)
            elif self.classifier == "classifier":
                return torch.optim.AdamW(self.model.classifier.parameters())
        else:
            print("Optimizing everything")
            return torch.optim.AdamW(self.parameters()) if not self.sgd else torch.optim.SGD(self.parameters(),
                                                                                            lr=self.lr)

    def train_dataloader(self):
        return self.loaders[0]

    def val_dataloader(self):
        return self.loaders[1]

    def test_dataloader(self):
        return self.loaders[2]

    def reset_loaders(self, root, batch_size=None, resize=None):
        resize = self.resize if resize is None else resize
        batch_size = self.batch_size if batch_size is None else batch_size
        self.loaders = create_attribute_dataloader(self.batch_size, resize=resize, root=root)
