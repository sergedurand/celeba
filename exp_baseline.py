import pathlib
from dataset import get_data_loader
import torch
import timm
from torchvision import models

from config import PROJ_DIR, \
    transform
from models.simple_net import Net, \
    NetFeat
from attacks.FGSM import FGSM
from utils import evaluate_vanilla, \
    train, \
    changeNumClass, \
    train_adv, \
    evaluate, \
    evaluate_attack
from attacks.PGD import PGD
from pathlib import Path
import random
from models.ensemble import Ensemble
from tqdm import tqdm


model = Net()
atk = PGD(model, torch.nn.BCEWithLogitsLoss(), num_iterations=2)

train(
        model=model,
        root=PROJ_DIR / "data/celeba_subset",
        bootstrap=10000,
        name="lastexp_ecaresnet_vanilla",
        max_test=1000,
        max_epochs=100,
        batch_size=16,

)

evaluate_vanilla(model.cuda(), step=20, limit=1000)


model = Net()
atk = PGD(model, torch.nn.BCEWithLogitsLoss(), num_iterations=2)

train_adv(
        model=model,
        root=PROJ_DIR / "data/celeba_subset",
        bootstrap=10000,
        name="lastexp_ecaresnet_ATTA",
        max_test=1000,
        val_atk=atk,
        attack=atk,
        sum_loss=False,
        threshold=0,
        save_attack=True,
        monitor="adv_val_loss",
        max_epochs=100,
        batch_size=16,
        weights=(1, 1),
        accumulate_grad_batches=1,
        reset=4,
        saved_pert_folder=PROJ_DIR / "data/ATTA",
)

evaluate_vanilla(model.cuda(), step=20, limit=1000)


model = Net()
atk = PGD(model, torch.nn.BCEWithLogitsLoss(), num_iterations=20)

train_adv(
        model=model,
        root=PROJ_DIR / "data/celeba_subset",
        bootstrap=10000,
        name="lastexp_ecaresnet_PGD_train",
        max_test=1000,
        val_atk=atk,
        attack=atk,
        sum_loss=False,
        threshold=0,
        save_attack=False,
        monitor="adv_val_loss",
        max_epochs=100,
        batch_size=16,
        weights=(1, 1),
        accumulate_grad_batches=4,
)

evaluate_vanilla(model.cuda(), step=20, limit=1000)
