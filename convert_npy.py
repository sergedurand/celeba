from numpy.lib.npyio import save
from config import PROJ_DIR
from PIL import Image
from pathlib import Path
from dataset import get_data_loader
import numpy as np
from tqdm import tqdm

imgs = PROJ_DIR / "data/celeba_subset/celeba_adv_PGD"
imgs = imgs.glob("*.npy")
# loader = get_data_loader(8,"all",root=PROJ_DIR / "data",resize=224)
print("here")
imgs = list(imgs)
for i in tqdm(range(len(imgs))):
    img = imgs[i]
    array = np.load(img)
    array = array.transpose(1,2,0) * 255
    array = array.astype(np.uint8)
    save_img = Image.fromarray(array)
    save_name = str(img.parent) + "_compressed/{}".format(img.stem)
    save_name = Path(save_name)
    if not save_name.parent.is_dir():
        save_name.parent.mkdir()
    save_img.save(save_name)