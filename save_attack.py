from pytorch_lightning.trainer.training_tricks import EPSILON_FP16
from torch.autograd.grad_mode import no_grad
from attacks.SMIA import SMIA
from models.simple_net import Net
from pathlib import Path
from attacks.FGSM import FGSM
from attacks.BIM import BIM
from attacks.PGD import PGD
from trainer import CelebAModel
from config import PROJ_DIR
from dataset import get_data_loader
import torch
from attacks.PGD import PGD
import numpy as np
from tqdm import tqdm
from config import TARGET
from PIL import Image

def save_attack(loader, model, atk,atk_name=None,save_dir = None):
    dataset = loader.dataset.data
    if atk_name is None:
        atk_name = type(atk).__name__
    model = model.cuda()
    save_folder = dataset.root
    cpt = 0
    for index, imgs, labels in tqdm(loader):
        img_names = dataset.filename[index]
        imgs_names = [name.split(".")[0] for name in img_names]
        # labels = torch.stack(imgs.shape[0]*[TARGET])
        img = Path(img_names[-1])
        if save_dir is None:
            save_name = str(save_folder) + "_{}/{}".format(atk_name,img.name)
        else:
            save_name = save_dir / img.name
        if Path(save_name).is_file():
            print("Skipping {}".format(save_name.name))
            continue
        adv_imgs = atk.attack(imgs.cuda(),labels.cuda())
        adv_imgs = adv_imgs.detach().clone().cpu().numpy()

        for i, img in enumerate(adv_imgs):
            array = img.transpose(1,2,0) * 255
            array = array.astype(np.uint8)
            save_img = Image.fromarray(array)
            if save_dir is None:
                save_name = str(save_folder) + "_{}/{}".format(atk_name,Path(img_names[i]).name)
                save_name = Path(save_name)
            else:
                save_name = save_dir / Path(img_names[i]).name
            if not save_name.parent.is_dir():
                save_name.parent.mkdir()
            save_img.save(save_name)
        cpt += imgs.shape[0]
    

def save_all_attacks(loader,model,save_folder_prefix):
    atk = PGD(model,loss_fn=torch.nn.BCEWithLogitsLoss(),save_folder=None,num_iterations=20, epsilon=8 / 255)
    save_attack(loader,model,atk,atk_name=save_folder_prefix + "_PGD_20")
    atk = FGSM(model,save_folder=None,loss_fn=torch.nn.BCEWithLogitsLoss())
    save_attack(loader,model,atk,atk_name=save_folder_prefix + "_FGSM")
    atk = BIM(model,save_folder=None,loss_fn=torch.nn.BCEWithLogitsLoss())
    save_attack(loader,model,atk,atk_name=save_folder_prefix + "_BIM_40")
    atk = SMIA(model,save_folder=None,loss_fn=torch.nn.BCEWithLogitsLoss(),a2=1)
    save_attack(loader,model,atk,atk_name=save_folder_prefix + "_SMIA_20_A2_1")
    atk = SMIA(model,save_folder=None,loss_fn=torch.nn.BCEWithLogitsLoss(),a2=0)
    save_attack(loader,model,atk,atk_name=save_folder_prefix + "_SMIA_20_A2_0")
    
# loader = get_data_loader(8,"all",root=PROJ_DIR / "data/celeba_subset",resize=224,shuffle=False)
# model = torch.load("/tempory/celeba/resnet50_subset.pth")
# atk = PGD(model,loss_fn=torch.nn.BCEWithLogitsLoss(),save_folder=None,num_iterations=20)

# save_attack(loader,model,atk,atk_name="RESNET_50_subset_no_adv_train")

<<<<<<< HEAD
loader = get_data_loader(4,"all",root=PROJ_DIR / "data/",resize=224,shuffle=False,num_workers=4)
model = torch.load("/tempory/celeba/all_data_resnext101.pth")
atk = PGD(model,loss_fn=torch.nn.BCEWithLogitsLoss(),save_folder=None,num_iterations=20, epsilon=8 / 255)
save_dir = Path("/tempory/celeba/data/celeba/celeba_adv_PGD_20_compressed")
save_attack(loader,model,atk,save_dir = save_dir)
=======
loader = get_data_loader(2,"test",root=PROJ_DIR / "data/celeba_subset",resize=224,shuffle=False,num_workers=4)
model = torch.load("/tempory/celeba/subset_models/baseline_subset.pth")
model = model.cuda()
save_all_attacks(loader,model,save_folder_prefix="samples_baseline")
>>>>>>> 90e46561e5ec5a9ba7f3234b725f493b5cac6759
